<?php

    require_once "libSmarty/Smarty.class.php";
    
    $smarty = new Smarty();
    $smarty->cache_dir   = "../smarty/cache";
    $smarty->config_dir  = "../smarty/config";
    $smarty->template_dir = "../smarty/tpl";
    $smarty->compile_dir = "../smarty/tplc";
