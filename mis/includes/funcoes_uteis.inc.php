<?php
error_reporting(0);
require_once "../includes/smarty.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();

function testa_campo($campo){
    $arr = array(
        "DROP",
        "DUMP",
        "DELETE",
        "CREATE",
        "ALTER",
        "UPDATE",
        "SELECT",
        "INSERT"
    );
    $retorno = "N";
    
    $campo  = explode(' ', $campo);
    
    foreach($arr as $value){
        foreach ($campo as $value2){
            if($value == strtoupper($value2)){
                $retorno = "S";
            }
        }
    }
    return $retorno;
}
function checa_autenticacao($usr, $senha){
    global $db;
	
	$sql = "SELECT COUNT(*) AS total FROM usuarios WHERE upper(usr) = upper('$usr') and senha = '$senha'";
      
	return $db->getOne($sql); 
}

function sair(){
    global $db;

    unset($_SESSION['usr']);
	unset($_SESSION['nome']);
	unset($_SESSION['senha']);
    session_destroy();
    $db->close();
    //header("Location: ../adm/login.php");  
}

function usr_perfil($usr_nome, $usr_email, $usr_senha){
    
	$usr_nome = ($usr_nome != '')?$usr_nome:$_SESSION['nome']; 
	$usr_email = ($usr_email != '')?$usr_email:$_SESSION['email']; 
	$usr_senha = ($usr_senha != '')?$usr_senha:$_SESSION['senha']; 

	
	$_SESSION['nome'] = $usr_nome;
	$_SESSION['email'] = $usr_email;
	$_SESSION['senha'] = $usr_senha;

}
function utf8_to_iso88591($string){
    $str_iso8859_1 = preg_replace("/([\xC2\xC3])([\x80-\xBF])/e", "chr(ord('\\1')<<6&0xC0|ord('\\2')&0x3F)", $string);													 
    return $str_iso8859_1;
}
function iso88591_to_utf8($string){
  $str_utf8 = preg_replace("/([\x80-\xFF])/e","chr(0xC0|ord('\\1')>>6).chr(0x80|ord('\\1')&0x3F)",$string);
  return $str_utf8;
}

function limpa($form, $campos, $div){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$objResponse->addScript("document.getElementById('".$form."').reset();");
	
	if($campos != 0){
		foreach($campos as $value){
			$objResponse->addScript("document.getElementById('".$value."').className = 'form-group'"); 
		}
	}
	
	if($div != ''){
		$objResponse->addScript("document.getElementById('div-msg-".$div."').className = 'alert alert-success display-hide'");    
		$objResponse->addScript("document.getElementById('msg-".$div."').innerHTML = ''");  
		$objResponse->addScript("document.getElementById('div-msg-".$div."').style.display = 'none'");
	}
    return $objResponse;
}

function criarLogExcluirProdutoCategoria($sku_produto, $nome_produto, $codigo_categoria, $nome_categoria){
    global $db;
    
	$arquivo = fopen(PATH_LOG.'log_excluir_produto_categoria.txt','a +');
	
	$usr  = $_SESSION['usr'];
	$nome = $_SESSION['nome'];
	
	if ($arquivo == true){
		$texto = date("d/m/Y H:i:s")." Log de Exclus�o de registro: Usuario: ".$usr." (".$nome.")  removeu a categoria  ".$nome_categoria." (".$codigo_categoria.") do produto ". $nome_produto." (".$sku_produto.")\n";
		fwrite($arquivo, $texto);
	}
	
	fclose($arquivo);
	
}

function criarLogExcluirProduto($sku_produto, $nome_produto){
    global $db;
    
	$arquivo = fopen(PATH_LOG.'log_excluir_produto.txt','a +');
	
	$usr  = $_SESSION['usr'];
	$nome = $_SESSION['nome'];
	
	if ($arquivo == true){
		$texto = date("d/m/Y H:i:s")." Log de Exclus�o de registro: Usuario: ".$usr." (".$nome.")  excluiu o Produto ". $nome_produto." (".$sku_produto.")\n";
		fwrite($arquivo, $texto);
	}
	
	fclose($arquivo);
	
}

function criarLogExcluirCategoria($codigo_categoria, $nome_categoria){
    global $db;
    
	$arquivo = fopen(PATH_LOG.'log_excluir_categoria.txt','a +');
	
	$usr  = $_SESSION['usr'];
	$nome = $_SESSION['nome'];
	
	if ($arquivo == true){
		$texto = date("d/m/Y H:i:s")." Log de Exclus�o de registro: Usuario: ".$usr." (".$nome.")  excluiu a Categoria ". $nome_categoria." (".$codigo_categoria.")\n";
		fwrite($arquivo, $texto);
	}
	
	fclose($arquivo);
	
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);



$smarty->assign("usr", $_SESSION['usr']);
$smarty->assign("usr_nome", $_SESSION['nome']);
