<?php
error_reporting(0);
require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();
///////////////////////////////////////////////////////////////////////////////
function login($aFormValues){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$usuario = (testa_campo($aFormValues['username']) == 'N')?$aFormValues['username']:'';
	$senha = (testa_campo($aFormValues['password']) == 'N')?$aFormValues['password']:'';
	$msg = '';
	$tipo = '';
	
	
	if((isset($usuario)&&$usuario!='')&&(isset($senha)&&$senha!='')){
		$sql = "SELECT nome, usr, senha FROM usuarios WHERE upper(usr) = upper('$usuario') and senha = md5('$senha')";
		$rs = $db->getRow($sql);
		//$objResponse->addAlert($sql);
		if($rs){
			
			$msg = 'Login efetuado com sucesso.';
			$tipo = 'alert-success';
			
			unset($_SESSION['usr']);
			unset($_SESSION['nome']);
			unset($_SESSION['senha']);
			
			$_SESSION['usr'] 		= $rs['usr'];
			$_SESSION['nome'] 		= $rs['nome'];
			$_SESSION['senha'] 		= $rs['senha'];
			
			$objResponse->addRedirect("home.php");
			
		}
		else{
			$msg = 'Usuário ou Senha Inválido.';
			$tipo = 'alert-danger';
		}
	}
	else{
		$msg = 'Usuário ou Senha Inválido.';
		$tipo = 'alert-danger';
	}
	
	if($msg!=''){
		$objResponse->addScript("document.getElementById('div-msg').className = 'alert $tipo display-block'");    
		$objResponse->addScript("document.getElementById('msg').innerHTML = '".$msg."'");  
		$objResponse->addScript("document.getElementById('div-msg').style.display = 'block'");
	}
	
    return $objResponse;
}

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////
////Fim das funçoes xajax////
/////////////////////////////

//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);

$img_login = array();
$img_login[] = '"../'.PATH_LOGIN.'bg1.jpg",';
$img_login[] = '"../'.PATH_LOGIN.'bg2.jpg",';
$img_login[] = '"../'.PATH_LOGIN.'bg3.jpg",';
$img_login[] = '"../'.PATH_LOGIN.'bg4.jpg",';
$img_login[] = '"../'.PATH_LOGIN.'bg5.jpg",';
//$img_login[] = '"../'.PATH_LOGIN.'bg6.jpg",';
//$img_login[] = '"../'.PATH_LOGIN.'bg7.jpg",';
//$img_login[] = '"../'.PATH_LOGIN.'bg8.jpg"';
$smarty->assign('img_login',$img_login);

$smarty->assign('img_login_fade',1000);
$smarty->assign('img_login_duration',5000);


// Cria objeto XAjax
$xajax = new xajax();
$xajax->setCharEncoding('utf-8');		// IMPORTANTE!
//$xajax->registerFunction('logar');
$xajax->registerFunction('login');
$xajax->registerFunction('msg_login');
$xajax -> processRequests();
$smarty->assign('xajax_javascript', $xajax->getJavascript('../includes/xajax/'));

$smarty->display("login.tpl");

