<?php
error_reporting(E_ALL);
require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();
///////////////////////////////////////////////////////////////////////////////
function add($aFormValues, $id = 0){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$nome 			= 	(testa_campo($aFormValues['nome']) == 'N')? $aFormValues['nome']: '';	
	
	$msg_arr 		= 	array();
	$msg 			= 	'';
	$err 			= 	0;
	$redirect 		= 	'N';
	
	if($nome == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: Nome';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('add-nome').className = 'form-group has-error col-sm-12'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('add-nome').className = 'form-group col-sm-12'"); 
	}
	
	if($err == 0){

		
		$sql =  "INSERT INTO categoria(nome)  VALUES (". $db->quote($nome). ")";
		//$objResponse->addAlert($sql); 
			
		if($db->execute($sql)){
			$msg_arr[] = 'Salvo com Sucesso.';
			$tipo_ = 'alert-success';
			$redirect = 'S';
		}else{
			$err++;
			$msg_arr[] = 'Erro inesperado ao salvar o Categoria.';
			$tipo_ = 'alert-danger';
			$redirect = 'N';
		}
		
	}
	
	
	if($msg_arr){
		foreach($msg_arr as $value){
			$msg .= $value;
			$msg .= (count($msg_arr) > 1)?'<br>':'';
		}
		
		$objResponse->addScript("document.getElementById('div-msg-add').className = 'alert $tipo_ display-block'");     
		$objResponse->addScript("document.getElementById('msg-add').innerHTML = '".$msg."'");  
		$objResponse->addScript("document.getElementById('div-msg-add').style.display = 'block'");
		$objResponse->addScript("oculta_msg('div-msg-add');");
	}
	
	
	if($redirect == 'S'){
		//$objResponse->addRedirect("grid_clientes.php");
		$objResponse->addScript("document.getElementById('btn_salvar').style.display = 'none';");
		$objResponse->addScript("setTimeout(function() {window.location.assign('cd_categoria.php?C=".md5($sku)."');}, 2000);");
	}
	
    return $objResponse;
}


function edit($aFormValues, $id = 0){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');

	$id_md5			= 	(testa_campo($aFormValues['id_md5']) == 'N')? $aFormValues['id_md5']: '';
	$codigo			= 	(testa_campo($aFormValues['codigo']) == 'N')? $aFormValues['codigo']: '';
	$nome 			= 	(testa_campo($aFormValues['nome']) == 'N')? $aFormValues['nome']: '';
	
	$msg_arr 		= 	array();
	$msg 			= 	'';
	$err 			= 	0;
	$redirect 		= 	'N';

	if($nome == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: Nome';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('edit-nome').className = 'form-group has-error col-sm-12'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('edit-nome').className = 'form-group col-sm-12'"); 
	}
	
	if($err == 0){
		
		$preco = str_replace('.','',$preco);
		$preco = str_replace(',','.',$preco);
		
		$sql =  "update categoria set nome = ".$db->quote($nome). " where codigo = ".$sku." and md5(codigo) = ".$db->quote($id_md5);
		//$objResponse->addAlert($sql); 
			
		if($db->execute($sql)){
			$msg_arr[] = 'Salvo com Sucesso.';
			$tipo_ = 'alert-success';
			$redirect = 'S';
		}else{
			$err++;
			$msg_arr[] = 'Erro inesperado ao salvar o Categoria.';
			$tipo_ = 'alert-danger';
			$redirect = 'N';
		}
		
	}
	
	
	if($msg_arr){
		foreach($msg_arr as $value){
			$msg .= $value;
			$msg .= (count($msg_arr) > 1)?'<br>':'';
		}
		
		$objResponse->addScript("document.getElementById('div-msg-edit').className = 'alert $tipo_ display-block'");     
		$objResponse->addScript("document.getElementById('msg-edit').innerHTML = '".$msg."'");  
		$objResponse->addScript("document.getElementById('div-msg-edit').style.display = 'block'");
		$objResponse->addScript("oculta_msg('div-msg-edit');");
	}
	
	
	if($redirect == 'S'){
		//$objResponse->addRedirect("grid_clientes.php");
		$objResponse->addScript("document.getElementById('btn_salvar').style.display = 'none';");
		$objResponse->addScript("setTimeout(function() {window.location.assign('cd_categoria.php?C=".md5($sku)."');}, 2000);");
	}
	
    return $objResponse;
}

///////////////////////////////////////////////////////////////////////////////
//////Fim das funçoes xajax////
/////////////////////////////
//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);

//Checa autenticacao do usuario
if (!$total = checa_autenticacao($_SESSION['usr'], $_SESSION['senha'])){
  header("location: login.php");
  //die();
}

///////////////////////////////////////////////////////////////////////////////
$smarty->assign("active", "Produtos");

if($_GET['C']){
	$sql = "select md5(codigo) id_md5, codigo, nome from categoria where md5(codigo) = ".$db->quote($_GET['C']);
	$rs = $db->getRow($sql);
	

	$smarty->assign("sql", $sql);
	$smarty->assign("list", $rs);
	
	$smarty->assign("id_md5", $rs['id_md5']);
	$smarty->assign("codigo", $rs['codigo']);
	$smarty->assign("nome", $rs['nome']);
	

	$smarty->assign("titulo", utf8_to_iso88591("Editar Categoria"));
	$smarty->assign("tipo", "edit");
}
else{
	$smarty->assign("id_md5", '');
	$smarty->assign("codigo", '');	
	$smarty->assign("nome",'');
	
	$smarty->assign("titulo", utf8_to_iso88591("Adicionar Categoria"));
	$smarty->assign("tipo", "add");	
}

///////////////////////////////////////////////////////////////////////////////
// Cria objeto XAjax
$xajax = new xajax();
//$xajax->setCharEncoding('utf-8');		// IMPORTANTE!
$xajax->registerFunction('add');
$xajax->registerFunction('edit');

/**********************************/
$xajax->registerFunction('limpa');
$xajax->registerFunction('cep');
$xajax->registerFunction('sair');
$xajax -> processRequests();
$smarty->assign('xajax_javascript', $xajax->getJavascript('../includes/xajax/'));
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("conteudo","cd_categoria.tpl");
$smarty->display("index.tpl");

?>