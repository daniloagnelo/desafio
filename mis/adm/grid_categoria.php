<?php
//error_reporting(0);
require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();
///////////////////////////////////////////////////////////////////////////////
function del($aFormValues){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$cod_categoria = (testa_campo($aFormValues['cod_categoria']) == 'N')? $aFormValues['cod_categoria']: '';
	
	$sql = "select nome from categoria where codigo = ".$cod_categoria;
	$nome_categoria = $db->getOne($sql);

	$sql = "delete from categoria where codigo = ". $cod_categoria;	
	if($db->execute($sql)){	
	
		$sql = "delete from produto_categoria where cod_categoria = ". $cod_categoria;	
		$db->execute($sql);
		
		criarLogExcluirCategoria($cod_categoria, $nome_categoria);
		
		$objResponse->addScript("document.location.reload();");
	}	
	
    return $objResponse;
}

function popula_del($id){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$objResponse->addScript("document.getElementById('del_cod_categoria').value = '".$id."'");

	
    return $objResponse;
}

function PesquisarContato($texto){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$sql = "select a.sku,
							md5(a.sku) as id_md5,
							a.nome, 
							a.descricao, 
							a.quantidade,
							a.preco,
							a.img
			from 	produto a
			where 0=0
			";
	if($texto != ''){
		//$texto = str_replace(' ','%',$texto);
		$sql .= "and (";
		$sql .= "      upper(CONCAT(' ', a.sku, ' ')) like upper('%$texto%')";
		$sql .= " or upper(CONCAT(' ', a.nome, ' ')) like upper('%$texto%')";
		$sql .= " or upper(CONCAT(' ', a.descricao, ' ')) like upper('%$texto%')";
		$sql .= ")";
	}
	$sql .= " order by a.nome";
	$rs = $db->getAll($sql);
	
	//$objResponse->addAlert($sql);
	
	$html = "";
	$alterar = "";
	$exluir = "";
	
	
	foreach($rs as $row){

		$html .= "	
					<div class=\"col-md-3\" style=\"margin-bottom:30px;\">
						<div class=\"mt-widget-2\"  style=\"text-align:center;\">
							<div class=\"\" style=\"background-image: url('".(($row['img'] != '')?PATH_IMG_PRODUTOS.$row['img']:PATH_IMG_PRODUTOS.'imagem_default.jpg')."');background-size: 100% 100%; width:200px;height:200px;margin-left: auto;margin-right: auto; margin-top:10px;\"></div>
							
							<div class=\"mt-body\" style=\"padding:0px;margin-top:-100px;\">
								<h3 class=\"mt-body-title\">  ".$row['nome']."</h3>
								
								<div style=\"min-height: 110px;vertical-align:middle;\">
									<p style=\"color: #666;font-size: 13px;margin-top: 10px;padding: 0 10px;\">  ".$row['descricao']." </p>
								</div>
								
								<ul class=\"mt-body-stats\">
									<li class=\"font-green\">
										<i class=\"fa fa-cubes fa-lg\"></i>  ".$row['quantidade']."
									</li>	
									<li class=\"font-green\">
										<i class=\"fa fa-money fa-lg\"></i> ".number_format($row['preco'],2,",",".")."
									</li>									
								</ul>
								<div class=\"mt-body-actions\">
									<div class=\"btn-group btn-group btn-group-justified\">
										<a href=\"cd_produto.php?C=".$row['id_md5']."\" class=\"btn font-blue\">
											<i class=\"fa fa-edit fa-lg\"></i> editar 
										</a>
										<a href=\"javascript:;\" class=\"btn  font-red\" data-target=\"#del\" data-toggle=\"modal\" onclick=\"xajax_popula_del('".$row['sku']."');\">
											<i class=\"fa fa-trash fa-lg\"></i> excluir 
										</a>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				 ";
	}
	$objResponse->addAssign("list_contato", 'innerHTML', $html);
	
	//$objResponse->addAlert($sql);
	
    return $objResponse;
}

///////////////////////////////////////////////////////////////////////////////
//////Fim das funçoes xajax////
/////////////////////////////
//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);

//Checa autenticacao do usuario
if (!$total = checa_autenticacao($_SESSION['usr'], $_SESSION['senha'])){
  header("location: login.php");
  //die();
}
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("active", "Categorias");
$smarty->assign("titulo", "Categorias");


$sql = "select a.codigo,
							md5(a.codigo) as id_md5,
							a.nome
			from 	categoria a"; 
$sql .= " order by nome";

$rs = $db->getAll($sql);
$smarty->assign("list", $rs);
$smarty->assign("count_list", count($rs));
$smarty->assign("sql", $sql);

$smarty->assign("PATH_IMG_PRODUTOS", PATH_IMG_PRODUTOS);
$smarty->assign("imagem_default", PATH_IMG_PRODUTOS.'imagem_default.jpg');

///////////////////////////////////////////////////////////////////////////////
// Cria objeto XAjax
$xajax = new xajax();
//$xajax->setCharEncoding('utf-8');		// IMPORTANTE!
$xajax->registerFunction('del');
$xajax->registerFunction('popula_del');
$xajax->registerFunction('PesquisarContato');
/**********************************/
$xajax->registerFunction('limpa');
$xajax->registerFunction('cep');
$xajax->registerFunction('sair');
$xajax -> processRequests();
$smarty->assign('xajax_javascript', $xajax->getJavascript('../includes/xajax/'));
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("conteudo","grid_categoria.tpl"); 
$smarty->display("index.tpl");

