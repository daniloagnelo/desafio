<?php
error_reporting(E_ALL);
require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();
///////////////////////////////////////////////////////////////////////////////
function add($aFormValues, $id = 0){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$sku 			= 	(testa_campo($aFormValues['sku']) == 'N')? $aFormValues['sku']: '';
	$nome 			= 	(testa_campo($aFormValues['nome']) == 'N')? $aFormValues['nome']: '';
	$quantidade 			= 	(testa_campo($aFormValues['quantidade']) == 'N')? $aFormValues['quantidade']: '';
	$preco 			= 	(testa_campo($aFormValues['preco']) == 'N')? $aFormValues['preco']: '';
	$descricao 			= 	(testa_campo($aFormValues['descricao']) == 'N')? $aFormValues['descricao']: '';
	
	$name_img_prod	= 	(testa_campo($aFormValues['name_img_prod']) == 'N')? $aFormValues['name_img_prod']: '';
	$file_img_prod	= 	(testa_campo($aFormValues['file_img_prod']) == 'N')? $aFormValues['file_img_prod']: '';	
	
	
	$msg_arr 		= 	array();
	$msg 			= 	'';
	$err 			= 	0;
	$redirect 		= 	'N';

	
	if($sku == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: SKU';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('add-sku').className = 'form-group has-error col-sm-4'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('add-sku').className = 'form-group col-sm-4'"); 
	}
	
	$sql = "select count(*) from produto where sku = ".	$sku ;
	$count_sku = $db->getOne($sql);
	
	if($count_sku > 0){
		$err++;
		$msg_arr[] = 'Obrigatório: SKU já cadastrado';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('add-sku').className = 'form-group has-error col-sm-4'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('add-sku').className = 'form-group col-sm-4'"); 
	}
	
	if($nome == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: Nome';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('add-nome').className = 'form-group has-error col-sm-12'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('add-nome').className = 'form-group col-sm-12'"); 
	}
	
	if($err == 0){
		
		$preco = str_replace('.','',$preco);
		$preco = str_replace(',','.',$preco);
		
		$sql =  "INSERT INTO produto(sku, nome, quantidade, preco, descricao, img)
								  VALUES (".   $sku.
										  ", ".(($nome != '')?$db->quote($nome):'null').
										  ", ".(($quantidade != '')?$db->quote($quantidade):'null').
										  ", ".(($preco != '')?$db->quote($preco):'null').
										  ", ".(($descricao != '')?$db->quote($descricao):'null').
										  ", ".(($name_img_prod != '')?$db->quote($name_img_prod):$db->quote('imagem_default.jpg')).
										")";
		//$objResponse->addAlert($sql); 
			
		if($db->execute($sql)){
			if($name_img_prod != ''){
				copy(PATH_IMG_TMP.$name_img_prod, PATH_IMG_PRODUTOS.$name_img_prod);
			
				$pasta = PATH_IMG_TMP;
				$pasta_produtos = PATH_IMG_PRODUTOS;

				if(is_dir($pasta)){
					$diretorio = dir($pasta);

					while($arquivo = $diretorio->read()){
						if(($arquivo != '.') && ($arquivo != '..')){
							unlink($pasta.$arquivo);
						}
					}
					
					$diretorio->close();
				}
				if($img_old != 'imagem_default.jpg'){
					unlink($pasta_produtos.$img_old);
				} 
			}
		
			$msg_arr[] = 'Salvo com Sucesso.';
			$tipo_ = 'alert-success';
			$redirect = 'S';
		}else{
			$err++;
			$msg_arr[] = 'Erro inesperado ao salvar o Produto.';
			$tipo_ = 'alert-danger';
			$redirect = 'N';
		}
		
	}
	
	
	if($msg_arr){
		foreach($msg_arr as $value){
			$msg .= $value;
			$msg .= (count($msg_arr) > 1)?'<br>':'';
		}
		
		$objResponse->addScript("document.getElementById('div-msg-add').className = 'alert $tipo_ display-block'");     
		$objResponse->addScript("document.getElementById('msg-add').innerHTML = '".$msg."'");  
		$objResponse->addScript("document.getElementById('div-msg-add').style.display = 'block'");
		$objResponse->addScript("oculta_msg('div-msg-add');");
	}
	
	
	if($redirect == 'S'){
		//$objResponse->addRedirect("grid_clientes.php");
		$objResponse->addScript("document.getElementById('btn_salvar').style.display = 'none';");
		$objResponse->addScript("setTimeout(function() {window.location.assign('cd_produto.php?C=".md5($sku)."');}, 2000);");
	}
	
    return $objResponse;
}


function edit($aFormValues, $id = 0){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');

	$id_md5			= 	(testa_campo($aFormValues['id_md5']) == 'N')? $aFormValues['id_md5']: '';
    
	$sku 			= 	(testa_campo($aFormValues['sku']) == 'N')? $aFormValues['sku']: '';
	$nome 			= 	(testa_campo($aFormValues['nome']) == 'N')? $aFormValues['nome']: '';
	$quantidade 			= 	(testa_campo($aFormValues['quantidade']) == 'N')? $aFormValues['quantidade']: '';
	$preco 			= 	(testa_campo($aFormValues['preco']) == 'N')? $aFormValues['preco']: '';
	$descricao 			= 	(testa_campo($aFormValues['descricao']) == 'N')? $aFormValues['descricao']: '';
	
	$name_img_prod	= 	(testa_campo($aFormValues['name_img_prod']) == 'N')? $aFormValues['name_img_prod']: '';
	$file_img_prod	= 	(testa_campo($aFormValues['file_img_prod']) == 'N')? $aFormValues['file_img_prod']: '';	
	
	
	$msg_arr 		= 	array();
	$msg 			= 	'';
	$err 			= 	0;
	$redirect 		= 	'N';

	
	if($sku == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: SKU';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('edit-sku').className = 'form-group has-error col-sm-4'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('edit-sku').className = 'form-group col-sm-4'"); 
	}
	
	$sql = "select count(*) from produto where sku = ".	$sku ;
	$count_sku = $db->getOne($sql);
	
	if($nome == ''){
		$err++;
		$msg_arr[] = 'Obrigatório: Nome';
		$tipo_ = 'alert-danger';
		$objResponse->addScript("document.getElementById('edit-nome').className = 'form-group has-error col-sm-12'"); 
	}
	else{
		$objResponse->addScript("document.getElementById('edit-nome').className = 'form-group col-sm-12'"); 
	}
	
	if($err == 0){
		
		$preco = str_replace('.','',$preco);
		$preco = str_replace(',','.',$preco);
		
		$sql =  "update produto set ".
					"  sku = ".(($sku != '')?$db->quote($sku):'null'). 
					"  ,nome = ".(($nome != '')?$db->quote($nome):'null'). 
					"  ,quantidade = ".(($quantidade != '')?$db->quote($quantidade):'null'). 
					"  ,preco = ".(($preco != '')?$db->quote($preco):'null'). 
					"  ,descricao = ".(($descricao != '')?$db->quote($descricao):'null'). 
					(($name_img_prod != '')?" ,img = ".(($name_img_prod != '')?$db->quote($name_img_prod):'imagem_default.jpg'):"").
					" ".
					"where sku = ".$sku." and md5(sku) = ".$db->quote($id_md5);
		//$objResponse->addAlert($sql); 
			
		if($db->execute($sql)){
			if($name_img_prod != ''){
				copy(PATH_IMG_TMP.$name_img_prod, PATH_IMG_PRODUTOS.$name_img_prod);
			
				$pasta = PATH_IMG_TMP;
				$pasta_produtos = PATH_IMG_PRODUTOS;

				if(is_dir($pasta)){
					$diretorio = dir($pasta);

					while($arquivo = $diretorio->read()){
						if(($arquivo != '.') && ($arquivo != '..')){
							unlink($pasta.$arquivo);
						}
					}
					
					$diretorio->close();
				}
				if($img_old != 'imagem_default.jpg'){
					unlink($pasta_produtos.$img_old);
				} 
			}
		
			$msg_arr[] = 'Salvo com Sucesso.';
			$tipo_ = 'alert-success';
			$redirect = 'S';
		}else{
			$err++;
			$msg_arr[] = 'Erro inesperado ao salvar o Produto.';
			$tipo_ = 'alert-danger';
			$redirect = 'N';
		}
		
	}
	
	
	if($msg_arr){
		foreach($msg_arr as $value){
			$msg .= $value;
			$msg .= (count($msg_arr) > 1)?'<br>':'';
		}
		
		$objResponse->addScript("document.getElementById('div-msg-edit').className = 'alert $tipo_ display-block'");     
		$objResponse->addScript("document.getElementById('msg-edit').innerHTML = '".$msg."'");  
		$objResponse->addScript("document.getElementById('div-msg-edit').style.display = 'block'");
		$objResponse->addScript("oculta_msg('div-msg-edit');");
	}
	
	
	if($redirect == 'S'){
		//$objResponse->addRedirect("grid_clientes.php");
		$objResponse->addScript("document.getElementById('btn_salvar').style.display = 'none';");
		$objResponse->addScript("setTimeout(function() {window.location.assign('cd_produto.php?C=".md5($sku)."');}, 2000);");
	}
	
    return $objResponse;
}

function incCategoria($sku, $cod_categoria){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
	
	$sql = "select count(*) from produto_categoria where sku = ".$sku." and cod_categoria = ".$cod_categoria;
	$count_produto_categoria = $db->getOne($sql);

	if($count_produto_categoria == 0){
	
		$sql = "insert into produto_categoria(sku, cod_categoria) values(".$sku.",".$cod_categoria.")";
		//$objResponse->addAlert($sql);
		if($db->execute($sql)){
			$objResponse->addScript("document.getElementById('div-msg-edit').className = 'alert alert-success display-block'");     
			$objResponse->addScript("document.getElementById('msg-edit').innerHTML = 'Categoria salva com sucesso.'");  
			$objResponse->addScript("document.getElementById('div-msg-edit').style.display = 'block'");
			$objResponse->addScript("oculta_msg('div-msg-edit');");
			$objResponse->addScript("document.getElementById('btn_salvar_tel').style.display = 'none';");
			$objResponse->addScript("topo();");
			$objResponse->addScript("setTimeout(function() {window.location.assign('cd_produto.php?C=".md5($sku)."');}, 2000);");
		}
	}else{
		$objResponse->addScript("document.getElementById('div-msg-edit').className = 'alert alert-danger display-block'");     
		$objResponse->addScript("document.getElementById('msg-edit').innerHTML = 'Categoria Cadastrada Anteriormente.'");  
		$objResponse->addScript("document.getElementById('div-msg-edit').style.display = 'block'");
		$objResponse->addScript("oculta_msg('div-msg-edit');");
		$objResponse->addScript("topo();");
	}

	
    return $objResponse;
}

function del($aFormValues){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$codigo_categoria = (testa_campo($aFormValues['codigo_categoria']) == 'N')? $aFormValues['codigo_categoria']: '';	
	
	$sql = "select * from produto_categoria where codigo = ".$codigo_categoria;
	$produto_categoria = $db->getRow($sql);
	
	$sql = "select nome from produto where sku = ".$produto_categoria['sku'];
	$nome_produto = $db->getOne($sql);
	
	$sql = "select nome from categoria where codigo = ".$codigo_categoria;
	$nome_categoria = $db->getOne($sql);
	
	$sql = "DELETE FROM produto_categoria WHERE codigo = ". $codigo_categoria;
	
	if($db->execute($sql)){	
		criarLogExcluirProdutoCategoria($produto_categoria['sku'], $nome_produto, $codigo_categoria, $nome_categoria);
		
		$objResponse->addScript("document.location.reload();");
	}	
	
    return $objResponse;
}

function popula_del($codigo){
    global $db, $smarty;
    
    $objResponse = new xajaxResponse();
    $objResponse->setCharEncoding('utf-8');
    
	$objResponse->addScript("document.getElementById('del_codigo_categoria').value = '".$codigo."'");
	
    return $objResponse;
}

///////////////////////////////////////////////////////////////////////////////
//////Fim das funçoes xajax////
/////////////////////////////
//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);

//Checa autenticacao do usuario
if (!$total = checa_autenticacao($_SESSION['usr'], $_SESSION['senha'])){
  header("location: login.php");
  //die();
}

///////////////////////////////////////////////////////////////////////////////
$smarty->assign("active", "Produtos");

if($_GET['C']){
	$sql = "select md5(sku) id_md5, sku, nome, descricao, quantidade, preco, img from produto where md5(sku) = ".$db->quote($_GET['C']);
	$rs = $db->getRow($sql);
	

	$smarty->assign("sql", $sql);
	$smarty->assign("list", $rs);
	
	$smarty->assign("id_md5", $rs['id_md5']);
	$smarty->assign("sku", $rs['sku']);
	
	$smarty->assign("nome", $rs['nome']);
	$smarty->assign("descricao", $rs['descricao']);
	$smarty->assign("quantidade", $rs['quantidade']);
	$smarty->assign("preco", $rs['preco']);
	$smarty->assign("img", $rs['img']);
	
	
	$sql = "select codigo, sku, cod_categoria, (select nome from categoria where codigo = cod_categoria)  as nome_categoria from produto_categoria where sku = ".$rs['sku']." order by 1 desc";
	$rs_produto_categoria = $db->getAll($sql);
	$smarty->assign("list_produto_categoria", $rs_produto_categoria);

	$smarty->assign("titulo", utf8_to_iso88591("Editar Produto"));
	$smarty->assign("tipo", "edit");
}
else{
	$smarty->assign("id_md5", '');
	$smarty->assign("sku", '');
	
	$smarty->assign("nome",'');
	$smarty->assign("descricao", '');
	$smarty->assign("quantidade", '');
	$smarty->assign("preco", '');
	$smarty->assign("img", '');
	
	$smarty->assign("titulo", utf8_to_iso88591("Adicionar Produto"));
	$smarty->assign("tipo", "add");	
}

$sql = "select * from categoria order by 2 asc";
$rs_categoria = $db->getAll($sql);
$smarty->assign("list_categoria", $rs_categoria);


$smarty->assign("PATH_IMG_PRODUTOS", PATH_IMG_PRODUTOS);
$smarty->assign("imagem_default", PATH_IMG_PRODUTOS.'imagem_default.jpg');


///////////////////////////////////////////////////////////////////////////////
// Cria objeto XAjax
$xajax = new xajax();
//$xajax->setCharEncoding('utf-8');		// IMPORTANTE!
$xajax->registerFunction('add');
$xajax->registerFunction('edit');
$xajax->registerFunction('incCategoria');
$xajax->registerFunction('del');
$xajax->registerFunction('popula_del');


/**********************************/
$xajax->registerFunction('limpa');
$xajax->registerFunction('cep');
$xajax->registerFunction('sair');
$xajax -> processRequests();
$smarty->assign('xajax_javascript', $xajax->getJavascript('../includes/xajax/'));
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("conteudo","cd_produto.tpl");
$smarty->display("index.tpl");

?>