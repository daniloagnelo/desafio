<?php
//error_reporting(0);
require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";
session_start();
date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_TIME, 'pt_BR');
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//////Fim das funçoes xajax////
/////////////////////////////
//Conecta no Banco
$db = conecta(); 
$db->SetFetchMode(ADODB_FETCH_ASSOC);

//Checa autenticacao do usuario
if (!$total = checa_autenticacao($_SESSION['usr'], $_SESSION['senha'])){
  header("location: login.php");
  //die();
}
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("active", "Dashboard");


$sql = "SELECT a.sku,
							a.nome, 
							a.quantidade,
							a.preco
			FROM 	produto a
			order by a.nome"; 
$list = $db->getAll($sql);
$smarty->assign("list", $list);


$sql = "SELECT sum(ifnull(quantidade,0)) FROM produto"; 
$total_qtd = $db->getOne($sql);
$smarty->assign("total_qtd", $total_qtd);

$sql = "SELECT sum(ifnull(quantidade,0)+ifnull(preco,0)) FROM produto"; 
$total_rs = $db->getOne($sql);
$smarty->assign("total_rs", $total_rs);
///////////////////////////////////////////////////////////////////////////////
// Cria objeto XAjax
$xajax = new xajax();
$xajax->setCharEncoding('utf-8');		// IMPORTANTE!
$xajax->registerFunction('sair');
$xajax -> processRequests();
$smarty->assign('xajax_javascript', $xajax->getJavascript('../includes/xajax/'));
///////////////////////////////////////////////////////////////////////////////
$smarty->assign("conteudo","home.tpl");
$smarty->display("index.tpl");

?>