<?php

require_once "../includes/smarty.php";
require_once "../includes/funcoes_uteis.inc.php";
require_once "../includes/xajax/xajax.inc.php";
require_once "../includes/adodb_util.inc.php";
require_once "../includes/global.inc.php";

// Inicia sessao
session_start();


// Limpa variaveis de sessao
unset($_SESSION['usr']); 
unset($_SESSION['nome']);
unset($_SESSION['senha']);
session_destroy();
$db->close();

header("Location: login.php"); 

?>
