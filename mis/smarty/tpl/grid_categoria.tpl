<style>
	.thHand{
		cursor:pointer;	
	}
</style>
<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-flag font-dark"></i>
					<span class="caption-subject bold uppercase">{$active}</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">						
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_categoria.php'"> 
							<i class="fa fa-plus"></i>
							Categoria 
						</button>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								
								
								
							</div>
						</div>
					</div>
				</div>
				
                <div class="row" id="list_categoria">
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
							<thead>
								<tr>
									
									<!--th class="col-md-2 text-center"> # </th-->
									<th style="display:none"></th>
									<th class="col-md-8 text-center">Nome</th>
									<th class="col-md-2 text-center">Editar</th>
									<th class="col-md-2 text-center">Excluir</th>
								</tr>  
							</thead>
							<tbody>
								{section name=i loop=$list}
									<tr>
										<td style="display:none">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1">
												<span></span>
											</label>
										</td>
										<!--td>{$list_tel[i].id}</td-->
										<td class="text-center">{$list[i].nome}</td>
										<td class="text-center"><a href="cd_categoria.php?C={$list[i].id_md5}" class="btn font-blue"><i class="fa fa-edit fa-lg"></i> editar </a></td>
										<td class="text-center"><a href="javascript:;" class="btn  font-red" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('{$list[i].codigo}');"><i class="fa fa-trash fa-lg"></i> excluir </a></td>
									</tr>
								{/section}
							</tbody>
						</table>
					
				</div>
			   
			   
			</div>
		
		
		<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Categoria</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão da Categoria?</h4>
					<input type="hidden" name="cod_categoria" id="del_cod_categoria">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
{include file='script_footer.tpl'}
{literal}
	<script>
	
	
	</script>
{/literal}
<!-- End Footer-->