
<div class="row">
	
	<div class="col-lg-8 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart font-dark hide"></i>
					<span class="caption-subject font-dark bold uppercase">Lista de Produtos</span>
				</div>
			</div>
			<div class="portlet-body">
				{section name=i loop=$list}
				
				{/section}
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
					<thead>
						<tr>
							
							<!--th class="col-md-2 text-center"> # </th-->
							<th style="display:none"></th>
							<th class="col-md-2 text-center">#</th>
							<th class="col-md-6 text-center">Nome</th>
							<th class="col-md-2 text-center">Quantidade</th>
							<th class="col-md-2 text-center">Preço</th>
						</tr>  
					</thead>
					<tbody>
						{section name=i loop=$list}
							<tr>
								<td style="display:none">
									<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
										<input type="checkbox" class="checkboxes" value="1">
										<span></span>
									</label>
								</td>
								<!--td>{$list_tel[i].id}</td-->
								<td class="text-center">{$list[i].sku}</td>
								<td class="text-center">{$list[i].nome}</td>
								<td class="text-center">{$list[i].quantidade}</td>
								<td class="text-center"> R$ {$list[i].preco|number_format:2:",":"."}</td>
							</tr>
						{/section}
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
	
	
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 green">
			<div class="visual">
				<i class="fa fa-cubes" style="margin-left:1px;"></i>
			</div>
			<div class="details">
				<div class="number">
					 <span data-counter="counterup" data-value="{$total_qtd}">{$total_qtd}</span>
				</div>
				<div class="desc">Total de Protutos (QTD)</div>
			</div>
		</a>
	</div>
	
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 purple">
			<div class="visual">
				<i class="fa fa-money" style="margin-left:1px;"></i>
			</div>
			<div class="details">
				<div class="number">
					 R$ <span data-counter="counterup" data-value="{$total_rs|number_format:2:",":"."}">{$total_rs|number_format:2:",":"."}</span>
				</div>
				<div class="desc">Total de Protutos (R$)</div>
			</div>
		</a>
	</div>
	

</div>



{include file='script_footer.tpl'}
<!--script src="../includes/assets/pages/scripts/charts-amcharts.js" type="text/javascript"></script-->


<!-- script --------------------------------->

{$AmChart}


