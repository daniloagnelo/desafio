{literal}
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>
{/literal}

<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-flag font-dark"></i>
					<span class="caption-subject bold uppercase">{$titulo}</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="{$id_md5}"> 
					<input type="hidden" name="codigo" id="codigo" value="{$codigo}"> 
					<div class="alert alert-danger display-hide" id="div-msg-{$tipo}">
						<button class="close" data-close="alert"></button>
						<span id="msg-{$tipo}"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group col-sm-12" id="{$tipo}-nome">
								<label>Nome <span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="nome" id="{$tipo}_nome" class="form-control" maxlength="100" placeholder="" value="{$nome}" > 
								</div>
							</div>								
						</div>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['{$tipo}-codigo','{$tipo}-nome'], '{$tipo}');window.location.href='grid_categoria.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_{$tipo}(xajax.getFormValues('form-envio'), 0);topo();">Salvar</button>
					</div>
				</form>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Categoria</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão da Categoria?</h4>
					<input type="hidden" name="codigo_categoria" id="del_codigo_categoria">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->


{include file='script_footer.tpl'}
{literal}
	<!--------------------------------------------------------------------------------------------->
	<script src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
	<script src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
	<!--------------------------------------------------------------------------------------------->
	<script>
		
		function up_foto(file){
			$('#form-envio').ajaxForm({
				//url: '../includes/funcoes_uteis.inc.php',
				url: 'upload_img_prod.php',
				type: 'post',
				success: function(data){
					$('#name_img_prod').attr('value',data);
				}
			}).submit();
		};
	</script>
{/literal}
{$tipoPessoa}
	
<!-- End Footer-->