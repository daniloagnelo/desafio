{literal}
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>
{/literal}

<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase">{$titulo}</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="{$id_md5}"> 
					<div class="alert alert-danger display-hide" id="div-msg-{$tipo}">
						<button class="close" data-close="alert"></button>
						<span id="msg-{$tipo}"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group text-center" id="{$tipo}-img">
								<div class="form-group">
									<div class="fileinput fileinput-new" data-provides="fileinput">
									
										<div class="fileinput-new thumbnail" style="width: 100%; height: 200px;">
											<!--img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" style="width: 200px; height: 200px;" /--> 
											<img src="{if $img != ''}{$PATH_IMG_PRODUTOS}{$img}{else}{$imagem_default}{/if}" alt="" style="width: 100%; height: 200px;" /> 
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 200px;"> </div>
										<div style="margin-top:10px">
											<span class="btn default btn-file">
												<span class="fileinput-new"> Selecione uma Imagem </span>
												<span class="fileinput-exists"> Alterar </span>
												<input type="file" name="file_img_prod" id="file_img_prod" onchange="up_foto(this.value);"> 
												<input type="hidden" name="name_img_prod" id="name_img_prod"> 
											</span>
											<!--a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a-->
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="{$tipo}-nome">
								<label>Nome <span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="nome" id="{$tipo}_nome" class="form-control" maxlength="100" placeholder="" value="{$nome}" > 
								</div>
							</div>								
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-4" id="{$tipo}-sku">
								<label>SKU<span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="sku" id="{$tipo}_sku" class="form-control text-upper" maxlength="11" placeholder="" value="{$sku}" > 
								</div>
							</div>
							<div class="form-group col-sm-4" id="{$tipo}-quantidade">
								<label>Quantidade</label>
								<div class="input-icon right">
									
									<input type="text" name="quantidade" id="{$tipo}_quantidade" class="form-control text-upper" maxlength="11" placeholder="" value="{$quantidade}" > 
								</div>
							</div>
							<div class="form-group col-sm-4" id="{$tipo}-preco">
								<label>Preço</label>
								<div class="input-icon right">
									
									<input type="text" name="preco" id="{$tipo}_preco" class="form-control text-upper" maxlength="17" placeholder="" value="{$preco|number_format:2:',':'.'}" > 
								</div>
							</div>
							
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="{$tipo}-descricao">
								<label>Descrição</label>
								<div class="input-icon right">
									<textarea name="descricao" id="{$tipo}_descricao" class="form-control" rows="5" maxlength="500" style="resize: none;">{$descricao}</textarea>
								</div>
							</div>
							
						</div>
					</div>
					
					{if $tipo == 'edit'}
					<hr>
					<div class="row">						
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="{$tipo}-categoria">
									<select class="bs-select form-control" name="categoria" id="{$tipo}_categoria" data-show-subtext="true" data-live-search="true">
											
										<option value="0">Selecione uma Categoria</option>
										
										{section name=j loop=$list_categoria}
										<option value="{$list_categoria[j].codigo}" data-icon="icon-flag">{$list_categoria[j].nome}</option>
										{/section}
										
									</select>
							</div>								
						</div>			
						<div class="col-sm-2">
							<div class="form-group col-sm-12" >
								<div class="input-icon right">
									<button type="button" id="btn_salvar_tel" class="btn green" onclick="xajax_incCategoria({$sku}, document.getElementById('{$tipo}_categoria').value);"><i class="fa fa-plus"></i> Adcionar</button>
								</div>
							</div>								
						</div>		
					</div>
					<hr>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								
								<!--th class="col-md-2 text-center"> # </th-->
								<th style="display:none"></th>
								<th class="col-md-10 text-center">Categoria</th>
								<th class="col-md-2 text-center">Excluir</th>
							</tr>  
						</thead>
						<tbody>
							{section name=i loop=$list_produto_categoria}
								<tr>
									<td style="display:none">
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1">
											<span></span>
										</label>
									</td>
									<!--td>{$list_tel[i].id}</td-->
									<td class="text-center">{$list_produto_categoria[i].nome_categoria}</td>
									<td class="text-center"><a href="javascript:void(null);" class="fa fa-trash fa-lg font-red" style="text-decoration:none" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('{$list_produto_categoria[i].codigo}');" data-toggle2="popover" data-placement="top" data-trigger="hover" title="" data-content="Excluir" data-original-title="">  </a></td>
								</tr>
							{/section}
						</tbody>
					</table>
					{/if}
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['{$tipo}-sku','{$tipo}-nome','{$tipo}-quantidade','{$tipo}-preco','{$tipo}-descricao'], '{$tipo}');window.location.href='grid_produtos.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_{$tipo}(xajax.getFormValues('form-envio'), 0);topo();">Salvar</button>
					</div>
				</form>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Categoria</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão da Categoria?</h4>
					<input type="hidden" name="codigo_categoria" id="del_codigo_categoria">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->


{include file='script_footer.tpl'}
{literal}
	<!--------------------------------------------------------------------------------------------->
	<script src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
	<script src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
	<!--------------------------------------------------------------------------------------------->
	<script>
		
		function up_foto(file){
			$('#form-envio').ajaxForm({
				//url: '../includes/funcoes_uteis.inc.php',
				url: 'upload_img_prod.php',
				type: 'post',
				success: function(data){
					$('#name_img_prod').attr('value',data);
				}
			}).submit();
		};
	</script>
{/literal}
{$tipoPessoa}
	
<!-- End Footer-->