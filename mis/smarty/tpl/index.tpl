<!DOCTYPE html> 
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <!-- BEGIN HEAD --><head>
        
        <title>My Inventory System</title>
		{$xajax_javascript}
		<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"-->
		 

		<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../includes/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../includes/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../includes/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="../arquivos/login/favicon.ico" /> 
		<!------------------------------------------------------------------------------>
        <link href="../includes/assets/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
		
		<link href="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		<link href="../includes/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />

		<link href="../includes/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="../includes/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../includes/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="../includes/assets/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" id="cssteste"/>
		
		<link href="../includes/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />
		
		<link href="../includes/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
		
		<link href="../includes/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
		
		
		<link href="../includes/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
        
		<!------------------------------------------------------------------------------>
		<!--link href="../includes/assets/app_css/pp.css" rel="stylesheet" type="text/css" id="style_color" /-->
		<!------------------------------------------------------------------------------>
		{literal}
		<style type="text/css">
			.text-card{color: #666;font-size: 15px;margin-top: 10px;padding: 0 10px;}
		</style>
		{/literal}
	</head>
    <!-- END HEAD -->

    <!--body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"-->
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-container-bg-solid">
	
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
				
                    <!-- BEGIN LOGO -->
                    <div class="page-logo" >
                        <a href="home.php">
							<img src="../arquivos/logo/logo_reduzido_index.png" alt="logo" class="logo-default" style="width:100px;margin-top:5px;"/> 
						</a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->					
					
					
					
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span> </span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
					
					<!----------------------------------------------------------------------------------------------------------------------->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
						
                        <ul class="nav navbar-nav pull-right">
							<!----------------------------------------------------------------------------------------------------------------------->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username"> {$usr_nome} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li class="divider"> </li>
                                    <li>
                                        <!--a href="login.php" onclick="xajax_sair();"-->
                                        <a href="sair.php">
                                            <i class="icon-key" ></i> Sair </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!----------------------------------------------------------------------------------------------------------------------->
                        </ul>
						
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
					<!----------------------------------------------------------------------------------------------------------------------->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
			
			
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
			
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
			
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                   
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            
							
							
                            <!--li class="heading">
                                <h3 class="uppercase">>Home<</h3>
                            </li-->
                            
							<li class="nav-item {if $active == 'Dashboard'}active{/if}">
								<a href="home.php" class="nav-link">
									<i class="icon-bar-chart "></i> 
									<span class="title">Dashboard</span>
									{if $active == 'Dashboard'}<span class="selected"></span>{/if}
								</a>
							</li>
							
							<li class="nav-item {if $active == 'Categorias'}active{/if}">
								<a href="grid_categoria.php" class="nav-link">
									<i class="icon-flag"></i>  
									<span class="title">Categorias</span>
									{if $active == 'Categorias'}<span class="selected"></span>{/if}
								</a>
							</li>
							
							<li class="nav-item {if $active == 'Produtos'}active{/if}">
								<a href="grid_produtos.php" class="nav-link">
									<i class="icon-puzzle"></i> 
									<span class="title">Produtos</span>
									{if $active == 'Produtos'}<span class="selected"></span>{/if}
								</a>
							</li>
							

                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
					
                </div>
                <!-- END SIDEBAR -->
				
				
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                       
                        <!-- BEGIN PAGE BAR >
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                            </ul>
                        </div>
                        < END PAGE BAR -->
                        
						<!-- BEGIN PAGE TITLE-->
                        <!--h1 class="page-title"> Home
                            <small>pagina inicial</small>
                        </h1-->
                        <!-- END PAGE TITLE-->
						
                        <!-- END PAGE HEADER-->
                        

                        <div class="clearfix"></div>
					
					
						{include file=$conteudo}
						
						
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
				
              
				
				
            </div>
            <!-- END CONTAINER -->
			
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2020 &copy; Danilo Agnelo
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>

		
		

		
		

    </body>
	
</html>