<style>
	.thHand{
		cursor:pointer;	
	}
</style>
<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase">{$active}</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">						
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_produto.php'"> 
							<i class="fa fa-plus"></i>
							Produto 
						</button>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								
								
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-sm-12" id="add-pesquisar">
						<input type="text" name="pesquisar" id="pesquisar" class="form-control" maxlength="100" placeholder="Pesquisar" onkeyup="xajax_Pesquisar(this.value);"> 
					</div>		
				</div>
				<HR>
                <div class="row" id="list_produto">
						{section name=i loop=$list}
						<div class="col-md-3" style="margin-bottom:30px;">
							<div class="mt-widget-2"  style="text-align:center;">
								<div class="" style="background-image: url('{if $list[i].img != ''}{$PATH_IMG_PRODUTOS}{$list[i].img}{else}{$imagem_default}{/if}');background-size: 100% 100%; width:200px;height:200px;margin-left: auto;margin-right: auto; margin-top:10px;"></div>
								
								<div class="mt-body" style="padding:0px;margin-top:-100px;">
									<h3 class="mt-body-title"> {$list[i].nome}</h3>
									
									<!--p class="mt-body-description"> {$list[i].descricao|truncate:100:"...":true} </p-->
									<div style="min-height: 110px;vertical-align:middle;">
										<p style="color: #666;font-size: 13px;margin-top: 10px;padding: 0 10px;"> {$list[i].descricao|truncate:150:"...":true} </p>
									</div>
									
									<ul class="mt-body-stats">
										<li class="font-green">
											<i class="fa fa-cubes fa-lg"></i> {$list[i].quantidade}
										</li>	
										<li class="font-green">
											<i class="fa fa-money fa-lg"></i> {$list[i].preco|number_format:2:",":"."}
										</li>									
									</ul>
									<div class="mt-body-actions">
										<div class="btn-group btn-group btn-group-justified">
											<a href="cd_produto.php?C={$list[i].id_md5}" class="btn font-blue">
												<i class="fa fa-edit fa-lg"></i> editar 
											</a>
											<a href="javascript:;" class="btn  font-red" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('{$list[i].sku}');">
												<i class="fa fa-trash fa-lg"></i> excluir 
											</a>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						
						{/section}
						{if $count_list == 0}
							<h4 align="center">Nenhum Contato Cadastrado</h4>
						{/if}
					
				</div>
			   
			   
			</div>
		
		
		<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Produto</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Produto?</h4>
					<input type="hidden" name="sku" id="del_sku">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
{include file='script_footer.tpl'}
{literal}
	<script>
	
	
	</script>
{/literal}
<!-- End Footer-->