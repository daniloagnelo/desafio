<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <title>My Inventory System</title>
		{$xajax_javascript}
		<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"--> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../includes/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../includes/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../includes/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../includes/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../includes/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="../arquivos/login/favicon.ico" /> 
		{literal}
		<style>
			.logo{text-align: center;width:100%;}
		</style>
		{/literal}
	</head>
    <!-- END HEAD -->

    <!--body class=" login"-->
    <body>
        <!-- BEGIN : LOGIN PAGE 5-2 -->
        <div class="user-login-5">
            <div class="row bs-reset">
				
				
			
                <div class="col-md-6 login-container bs-reset">
				
                    <div class="login-content" style="margin-top:10%;">
						<div class="logo">
							<img class="" src="../arquivos/login/logo.png" width="350px" align="center"/>
						</div>
                        <!--h1 align="center">My Contacts System</h1-->
                        <!--p> Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p-->
                        <form action="javascript:void(null);" class="login-form" method="post" name="login" id="login" onsubmit="xajax_login(xajax.getFormValues('login'));">
                            <div class="alert alert-danger display-hide" id="div-msg">
                                <button class="close" data-close="alert"></button>
                                <span id="msg">Informe o Usuário e Senha. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Usuário" name="username" required/> 
								</div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Senha" name="password" required/> 
								</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10 text-right" style="vertical-align:middle;">
                                    <!--utton class="btn blue" type="submit" onclick="xajax_login(xajax.getFormValues('login'));">Entrar</button-->
									<!--a href="esqueceu_a_senha.php">Esqueceu a Senha?</a-->
                                </div>
                                <div class="col-sm-2 text-right" style="vertical-align:middle;">
                                    <!--utton class="btn blue" type="submit" onclick="xajax_login(xajax.getFormValues('login'));">Entrar</button-->
                                    <!--input class="btn blue" type="submit" onclick="xajax_login(xajax.getFormValues('login'));" value="Entrar"-->
                                    <input class="btn blue" type="submit" value="Entrar">
                                </div>
                            </div>
                        </form>
						
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-12 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; Danilo Agnelo 2020</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
				<div class="col-md-6 bs-reset">
                    <div class="login-bg"></div>
                </div>
				
            </div>
			
        </div>
        <!-- END : LOGIN PAGE 5-2 -->
        <!--[if lt IE 9]>
<script src="../includes/assets/global/plugins/respond.min.js"></script>
<script src="../includes/assets/global/plugins/excanvas.min.js"></script> 
<script src="../includes/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../includes/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../includes/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../includes/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../includes/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            });

		</script>
		
		
		<script>
				var Login = function() {

				var handleLogin = function() {

					$('.login-form').validate({
						errorElement: 'span', //default input error message container
						errorClass: 'help-block', // default input error message class
						focusInvalid: false, // do not focus the last invalid input
						rules: {
							username: {
								required: true
							},
							password: {
								required: true
							},
							remember: {
								required: false
							}
						},

						messages: {
							username: {
								required: "Username is required."
							},
							password: {
								required: "Password is required."
							}
						},

						invalidHandler: function(event, validator) { //display error alert on form submit   
							$('.alert-danger', $('.login-form')).show();
						},

						highlight: function(element) { // hightlight error inputs
							$(element)
								.closest('.form-group').addClass('has-error'); // set error class to the control group
						},

						success: function(label) {
							label.closest('.form-group').removeClass('has-error');
							label.remove();
						},

						errorPlacement: function(error, element) {
							error.insertAfter(element.closest('.input-icon'));
						},

						submitHandler: function(form) {
							form.submit(); // form validation success, call ajax form submit
						}
					});

					$('.login-form input').keypress(function(e) {
						if (e.which == 13) {
							if ($('.login-form').validate().form()) {
								$('.login-form').submit(); //form validation success, call ajax form submit
							}
							return false;
						}
					});

					$('.forget-form input').keypress(function(e) {
						if (e.which == 13) {
							if ($('.forget-form').validate().form()) {
								$('.forget-form').submit();
							}
							return false;
						}
					});

					$('#forget-password').click(function(){
						$('.login-form').hide();
						$('.forget-form').show();
					});

					$('#back-btn').click(function(){
						$('.login-form').show();
						$('.forget-form').hide();
					});
				}

			 
			  

				return {
					//main function to initiate the module
					init: function() {

						handleLogin();

						// init background slide images
						$('.login-bg').backstretch([
							{section name=i loop=$img_login}
								{$img_login[i]}
							{/section}
							], {
							  fade: {$img_login_fade},
							  duration: {$img_login_duration}
							}
						);

						$('.forget-form').hide();

					}

				};

			}();

			jQuery(document).ready(function() {
				Login.init();
			});
		</script>
		
    </body>

</html>