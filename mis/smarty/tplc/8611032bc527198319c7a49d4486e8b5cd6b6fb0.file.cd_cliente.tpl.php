<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 18:13:14
         compiled from "../smarty/tpl/cd_cliente.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9867983145da6179b1af7b2-96365956%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8611032bc527198319c7a49d4486e8b5cd6b6fb0' => 
    array (
      0 => '../smarty/tpl/cd_cliente.tpl',
      1 => 1571172826,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9867983145da6179b1af7b2-96365956',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da6179b426232_17847044',
  'variables' => 
  array (
    'titulo' => 0,
    'inc' => 0,
    'adm' => 0,
    'id_md5' => 0,
    'id_cliente' => 0,
    'tipo' => 0,
    'nome' => 0,
    'status' => 0,
    'natureza' => 0,
    'cnpj' => 0,
    'ie' => 0,
    'cpf' => 0,
    'rg' => 0,
    'ordem' => 0,
    'dia_visita' => 0,
    'periodo_visita' => 0,
    'hora_abertura' => 0,
    'hora_almoco_ini' => 0,
    'hora_almoco_fim' => 0,
    'hora_fechamento' => 0,
    'cep' => 0,
    'endereco' => 0,
    'numero' => 0,
    'complemento' => 0,
    'bairro' => 0,
    'cidade' => 0,
    'uf' => 0,
    'contato' => 0,
    'telefone' => 0,
    'celular' => 0,
    'email' => 0,
    'obs' => 0,
    'tipoPessoa' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da6179b426232_17847044')) {function content_5da6179b426232_17847044($_smarty_tpl) {?>
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>


<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_cliente.php'"> 
							<i class="fa fa-plus"></i>
							Cliente  
						</button>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="<?php echo $_smarty_tpl->tpl_vars['id_md5']->value;?>
"> 
					<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_smarty_tpl->tpl_vars['id_cliente']->value;?>
"> 
					<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
						<button class="close" data-close="alert"></button>
						<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
					</div>
					
					<div class="row">						
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome">
								<label>Nome<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="nome" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_nome" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
" > 
								</div>
							</div>								
						</div>
						<div class="col-sm-2">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-status">
								<label>Status</label>
								<div class="input-icon">
									
									<select class="bs-select form-control" data-show-subtext="true" data-style="btn green" name="status" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_status" >
										<option value="A" <?php if ($_smarty_tpl->tpl_vars['status']->value=='A') {?>selected<?php }?>>Ativo</option>
										<option value="I" <?php if ($_smarty_tpl->tpl_vars['status']->value=='I') {?>selected<?php }?>>Inativo</option>
									</select>
								</div>
							</div>						
						</div>						
					</div>
					<div class="row">						
						<div class="col-sm-4">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-natureza">
								<label>Natureza</label>
								<div class="input-icon right">
									<label class="mt-radio" style="padding-right:10px;">
										<input type="radio" name="natureza" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_naturezaJ" value="J" onchange="tipoPessoa(this.value)" <?php if ($_smarty_tpl->tpl_vars['natureza']->value=='J') {?>checked<?php }?>> Pessoa Juridica
										<span></span>
									</label>
									<label class="mt-radio" style="padding-right:10px;">
										<input type="radio" name="natureza" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_naturezaF" value="F" onchange="tipoPessoa(this.value)" <?php if ($_smarty_tpl->tpl_vars['natureza']->value=='F') {?>checked<?php }?>> Pessoa Física
										<span></span>
									</label>
								</div>								
							</div>								
						</div>
						
						<div id="pj" style="display:block">
							<div class="col-sm-4">
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cnpj">
									<label>CNPJ<span class="required"> * </span></label>
									<div class="input-icon right">
										<input type="text" name="cnpj" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_cnpj" class="form-control" maxlength="18" placeholder="" onkeydown="javascript: fMasc( this, mCNPJ );" value="<?php echo $_smarty_tpl->tpl_vars['cnpj']->value;?>
" > 
									</div>
								</div>								
							</div>	
							<div class="col-sm-4">
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-ie">
									<label>IE</label>
									<div class="input-icon right">
										<input type="text" name="ie" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_ie" class="form-control" maxlength="20" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ie']->value;?>
" > 
									</div>
								</div>								
							</div>									
						</div>
						<div id="pf" style="display:none">
							<div class="col-sm-4">
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cpf">
									<label>CPF<span class="required"> * </span></label>
									<div class="input-icon right">
										<input type="text" name="cpf" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_cpf" class="form-control" maxlength="14" placeholder="" onkeydown="javascript: fMasc( this, mCPF );" value="<?php echo $_smarty_tpl->tpl_vars['cpf']->value;?>
" > 
									</div>
								</div>								
							</div>	
							<div class="col-sm-4">
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-rg">
									<label>RG</label>
									<div class="input-icon right">
										<input type="text" name="rg" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_rg" class="form-control" maxlength="20" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['rg']->value;?>
" > 
									</div>
								</div>								
							</div>									
						</div>						
					</div>
					<HR>
					<div class="row">						
						<div class="col-sm-2">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-ordem">
								<label>Ordem<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="ordem" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_ordem" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ordem']->value;?>
" > 
								</div>
							</div>								
						</div>
						<div class="col-sm-5">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-dia_visita">
								<label>Dia Visita<span class="required"> * </span></label>
								<div class="input-icon">
									
									<select class="bs-select form-control" data-show-subtext="true" data-style="btn green" name="dia_visita" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_dia_visita" >
										<option value="null" >Selecione um dia da Semana</option>
										<option value="SEG" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='SEG') {?>selected<?php }?>>Segunda</option>
										<option value="TER" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='TER') {?>selected<?php }?>>Terça</option>
										<option value="QUA" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='QUA') {?>selected<?php }?>>Quarta</option>
										<option value="QUI" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='QUI') {?>selected<?php }?>>Quinta</option>
										<option value="SEX" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='SEX') {?>selected<?php }?>>Sexta</option>
										<option value="SAB" <?php if ($_smarty_tpl->tpl_vars['dia_visita']->value=='SAB') {?>selected<?php }?>>Sábado</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-periodo_visita">
								<label>Periodo Visita<span class="required"> * </span></label>
								<div class="input-icon">
									
									<select class="bs-select form-control" data-show-subtext="true" data-style="btn green" name="periodo_visita" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_periodo_visita" >
										<option value="null" >Selecione um Periodo para Visita</option>
										<option value="S" <?php if ($_smarty_tpl->tpl_vars['periodo_visita']->value=='S') {?>selected<?php }?>>Semanal</option>
										<option value="Q" <?php if ($_smarty_tpl->tpl_vars['periodo_visita']->value=='Q') {?>selected<?php }?>>Quinzenal</option>
									</select>
								</div>
							</div>
						</div>
						
					</div>
					<div class="row">						
						<div class="col-sm-3">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-hora_abertura">
								<label>Hora Abertura<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="hora_abertura" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_abertura" class="form-control" maxlength="5" placeholder="" onkeydown="javascript: fMasc( this, mHora );"value="<?php echo $_smarty_tpl->tpl_vars['hora_abertura']->value;?>
" > 
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-hora_almoco_ini">
								<label>Hora Almoço - Inicio<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="hora_almoco_ini" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_hora_almoco_ini" class="form-control" maxlength="5" placeholder="" onkeydown="javascript: fMasc( this, mHora );"value="<?php echo $_smarty_tpl->tpl_vars['hora_almoco_ini']->value;?>
" > 
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-hora_almoco_fim">
								<label>Hora Almoço - Fim<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="hora_almoco_fim" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_hora_almoco_fim" class="form-control" maxlength="5" placeholder="" onkeydown="javascript: fMasc( this, mHora );"value="<?php echo $_smarty_tpl->tpl_vars['hora_almoco_fim']->value;?>
" > 
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-hora_fechamento">
								<label>Hora Fechamento<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="hora_fechamento" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_hora_fechamento" class="form-control" maxlength="5" placeholder="" onkeydown="javascript: fMasc( this, mHora );"value="<?php echo $_smarty_tpl->tpl_vars['hora_fechamento']->value;?>
" > 
								</div>
							</div>
						</div>
					</div>
					<HR>
					<div class="row">
						<div class="col-sm-12">					
							<div class="form-group col-sm-3" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cep">
								<label class="control-label">CEP</label>
								<input type="text" name="cep" id="cep" maxlength="10" class="form-control" placeholder="Cep" onchange="" value="<?php echo $_smarty_tpl->tpl_vars['cep']->value;?>
" onkeydown="javascript: fMasc( this, mCep );">
							</div>
							<div class="form-group col-sm-3" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cep">
								<label class="control-label">&nbsp;</label>
								<div class="input-icon right">
								<button type="button" id="btn_salvar" class="btn green" onclick="xajax_BuscaCep(document.getElementById('cep').value);"><i class="fa fa-search"></i> Pesquisa CEP</button>
								</div>
							</div>
							
						</div>
						<div class="col-sm-10">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-endereco">
								<label class="control-label">Endereço<span class="required"> * </span></label>
									<input type="text" name="endereco" id="endereco" maxlength="250" class="form-control" placeholder="Endereço" value="<?php echo $_smarty_tpl->tpl_vars['endereco']->value;?>
">
							</div>
						</div>
						<div class="col-sm-2">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-numero">
								<label class="control-label">Numero<span class="required"> * </span></label>
									<input type="text" name="numero" id="numero" maxlength="10" class="form-control" placeholder="Numero" value="<?php echo $_smarty_tpl->tpl_vars['numero']->value;?>
">
							</div>
						</div>
						<div class="col-sm-6">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-complemento">
								<label class="control-label">Complemento</label>
									<input type="text" name="complemento" id="complemento" maxlength="150" class="form-control" placeholder="Complemento" value="<?php echo $_smarty_tpl->tpl_vars['complemento']->value;?>
">
							</div>
						</div>
						<div class="col-sm-6">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-bairro">
								<label class="control-label">Bairro<span class="required"> * </span></label>
									<input type="text" name="bairro" id="bairro" maxlength="150" class="form-control" placeholder="Bairro" value="<?php echo $_smarty_tpl->tpl_vars['bairro']->value;?>
">
							</div>
						</div>
						<div class="col-sm-10">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cidade">
								<label class="control-label">Cidade<span class="required"> * </span></label>
									<input type="text" name="cidade" id="cidade" maxlength="150" class="form-control" placeholder="Cidade" value="<?php echo $_smarty_tpl->tpl_vars['cidade']->value;?>
">
							</div>
						</div>
						<div class="col-sm-2">					
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-uf">
								<label class="control-label">UF<span class="required"> * </span></label>
									<input type="text" name="uf" id="uf" maxlength="2" class="form-control" placeholder="Estado (UF)" value="<?php echo $_smarty_tpl->tpl_vars['uf']->value;?>
">
							</div>
						</div>
								
					</div>
					<HR>
					<div class="row">						
						<div class="col-sm-4">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-contato">
								<label>Contato</label>
								<div class="input-icon right">
									<input type="text" name="contato" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_contato" class="form-control" maxlength="50" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['contato']->value;?>
" > 
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-telefone">
								<label>Telefone</label>
								<div class="input-icon right">
									<input type="text" name="telefone" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_telefone" class="form-control" maxlength="14" placeholder="" onkeydown="javascript: fMasc( this, mTel );" value="<?php echo $_smarty_tpl->tpl_vars['telefone']->value;?>
" > 
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-celular">
								<label>Celular</label>
								<div class="input-icon right">
									<input type="text" name="celular" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_celular" class="form-control" maxlength="15" placeholder="" onkeydown="javascript: fMasc( this, mCel );" value="<?php echo $_smarty_tpl->tpl_vars['celular']->value;?>
" > 
								</div>
							</div>
						</div>						
						<div class="col-sm-12">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-email">
								<label>Email</label>
								<div class="input-icon right">
									<input type="text" name="email" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_email" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" > 
								</div>
							</div>
						</div>					
						<div class="col-sm-12">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-obs">
								<label>Observação</label>
								<div class="input-icon right">
									<textarea name="obs" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_obs" class="form-control" rows="5" maxlength="500" style="resize: none;"><?php echo $_smarty_tpl->tpl_vars['obs']->value;?>
</textarea>
								</div>
							</div>
						</div>
					</div>
						
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-un','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-preco','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-status','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-descricao'], '<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');window.location.href='grid_clientes.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-envio'));topo();">Salvar</button>
					</div>
				</form>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	
	<?php echo '<script'; ?>
>
		
		function tipoPessoa(natureza){
			if(natureza == 'J'){
				document.getElementById('pf').style.display = "none";
				document.getElementById('pj').style.display = "block";
			}
			if(natureza == 'F'){
				document.getElementById('pf').style.display = "block";
				document.getElementById('pj').style.display = "none";
			}
		} 
	<?php echo '</script'; ?>
>	

<?php echo $_smarty_tpl->tpl_vars['tipoPessoa']->value;?>

	
<!-- End Footer--><?php }} ?>
