<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-16 17:34:30
         compiled from "../smarty/tpl/cd_produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7223576845da77ed6e3b0d3-11386734%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4daa7455eee07ac1b80d83b208eb44361b0b211' => 
    array (
      0 => '../smarty/tpl/cd_produto.tpl',
      1 => 1570880552,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7223576845da77ed6e3b0d3-11386734',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    'inc' => 0,
    'adm' => 0,
    'id_md5' => 0,
    'id_produto' => 0,
    'tipo' => 0,
    'img' => 0,
    'PATH_IMG_PRODUTOS' => 0,
    'imagem_default' => 0,
    'nome' => 0,
    'un' => 0,
    'preco' => 0,
    'status' => 0,
    'descricao' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da77ed717a659_30628396',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da77ed717a659_30628396')) {function content_5da77ed717a659_30628396($_smarty_tpl) {?>
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>


<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_produto.php'"> 
							<i class="fa fa-plus"></i>
							Produto 
						</button>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="<?php echo $_smarty_tpl->tpl_vars['id_md5']->value;?>
"> 
					<input type="hidden" name="id_produto" id="id_produto" value="<?php echo $_smarty_tpl->tpl_vars['id_produto']->value;?>
"> 
					<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
						<button class="close" data-close="alert"></button>
						<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group text-center" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-img">
								<div class="form-group">
									<div class="fileinput fileinput-new" data-provides="fileinput">
									
										<div class="fileinput-new thumbnail" style="width: 100%; height: 200px;">
											<!--img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" style="width: 200px; height: 200px;" /--> 
											<img src="<?php if ($_smarty_tpl->tpl_vars['img']->value!='') {
echo $_smarty_tpl->tpl_vars['PATH_IMG_PRODUTOS']->value;
echo $_smarty_tpl->tpl_vars['img']->value;
} else {
echo $_smarty_tpl->tpl_vars['imagem_default']->value;
}?>" alt="" style="width: 100%; height: 200px;" /> 
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 200px;"> </div>
										<div style="margin-top:10px">
											<span class="btn default btn-file">
												<span class="fileinput-new"> Selecione uma Imagem </span>
												<span class="fileinput-exists"> Alterar </span>
												<input type="file" name="file_img_prod" id="file_img_prod" onchange="up_foto(this.value);"> 
												<input type="hidden" name="name_img_prod" id="name_img_prod"> 
											</span>
											<!--a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a-->
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome">
								<label>Nome<span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="nome" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_nome" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
" > 
								</div>
							</div>								
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-3" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-un">
								<label>Unidade<span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="un" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_un" class="form-control text-upper" maxlength="3" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['un']->value;?>
" > 
								</div>
							</div>
							<div class="form-group col-sm-5" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-preco">
								<label>Preço<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="preco" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_preco" class="form-control" maxlength="17" placeholder="" value="<?php echo number_format($_smarty_tpl->tpl_vars['preco']->value,2,',','.');?>
"> 
								</div>
							</div>
							<div class="form-group col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-status">
								<label>Status</label>
								<div class="input-icon">
									
									<select class="bs-select form-control" data-show-subtext="true" data-style="btn green" name="status" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_status" >
										<option value="A" <?php if ($_smarty_tpl->tpl_vars['status']->value=='A') {?>selected<?php }?>>Ativo</option>
										<option value="I" <?php if ($_smarty_tpl->tpl_vars['status']->value=='I') {?>selected<?php }?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-descricao">
								<label>Descrição</label>
								<div class="input-icon right">
									<textarea name="descricao" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_descricao" class="form-control" rows="5" maxlength="500" style="resize: none;"><?php echo $_smarty_tpl->tpl_vars['descricao']->value;?>
</textarea>
								</div>
							</div>
							
						</div>
					</div>
						
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-un','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-preco','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-status','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-descricao'], '<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');window.location.href='grid_produtos.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-envio'));topo();">Salvar</button>
					</div>
				</form>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
 src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"><?php echo '</script'; ?>
>
	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
>
		
		function up_foto(file){
			$('#form-envio').ajaxForm({
				//url: '../includes/funcoes_uteis.inc.php',
				url: 'upload_img_prod.php',
				type: 'post',
				success: function(data){
					$('#name_img_prod').attr('value',data);
				}
			}).submit();
		};
	<?php echo '</script'; ?>
>

<!-- End Footer--><?php }} ?>
