<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:28:40
         compiled from "../smarty/tpl/grid_pedidos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10629321465da60fd8161719-73846797%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8a6af5ce3eb4fc80d9d023ebaa9aee998aebec1' => 
    array (
      0 => '../smarty/tpl/grid_pedidos.tpl',
      1 => 1571145156,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10629321465da60fd8161719-73846797',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    'inc' => 0,
    'adm' => 0,
    'alt' => 0,
    'exc' => 0,
    'list' => 0,
    'corStatus' => 0,
    'texto' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da60fd82250a5_83519006',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da60fd82250a5_83519006')) {function content_5da60fd82250a5_83519006($_smarty_tpl) {?><div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_pedido.php'"> 
							<i class="fa fa-plus"></i>
							Pedido 
						</button>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								
							</div>
						</div>
					</div>
				</div>
				
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
					<thead>
						<tr>
							<th style="display:none">
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
									<span></span>
								</label>
							</th>
							<th class="col-md-1"> # </th>
							<th class="col-md-2">Data</th>
							<th class="col-md-4">Cliente</th>
							<th class="col-md-2">Valor</th>
							<th class="col-md-1">Status</th>
							<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><th class="col-md-1"></th><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><th class="col-md-1"></th><?php }?>

						</tr>  
					</thead>
					<tbody>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<tr class="odd gradeX">
							<td style="display:none" id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_0">
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="checkboxes" value="1" />
									<span></span>
								</label>
							</td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_1" class="text-right"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
 </td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_2" class="text-center"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data'];?>
</td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_3" class="text-left"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['nome_cli'];?>
 </td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_4" class="text-right"> R$ <?php echo number_format($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['valor_pedido'],2,",",".");?>
 </td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
_5" class="text-center"> 
								<?php $_smarty_tpl->tpl_vars["texto"] = new Smarty_variable('', null, 0);?>
								<?php $_smarty_tpl->tpl_vars["corStatus"] = new Smarty_variable('', null, 0);?>
								<?php $_smarty_tpl->tpl_vars["icone"] = new Smarty_variable('', null, 0);?>
								
								<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status']=='D') {
$_smarty_tpl->tpl_vars["texto"] = new Smarty_variable("Digitado", null, 0);
$_smarty_tpl->tpl_vars["corStatus"] = new Smarty_variable("blue", null, 0);
}?>
								<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status']=='F') {
$_smarty_tpl->tpl_vars["texto"] = new Smarty_variable("Finalizado", null, 0);
$_smarty_tpl->tpl_vars["corStatus"] = new Smarty_variable("green", null, 0);
}?>
								
								<a href="javascript:void(null);" class="font-<?php echo $_smarty_tpl->tpl_vars['corStatus']->value;?>
 popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo $_smarty_tpl->tpl_vars['texto']->value;?>
" data-original-title=""><i class="fa fa-history fa-lg font-<?php echo $_smarty_tpl->tpl_vars['corStatus']->value;?>
"></i> <?php echo $_smarty_tpl->tpl_vars['texto']->value;?>
</a> 
							</td>
							<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
							<td align="center"> 
								<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status']=='D') {?>
									<a href="cd_pedido.php?cod=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_md5'];?>
&cli=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cliente'];?>
" class="fa fa-edit fa-lg font-blue popovers" style="text-decoration:none" data-container="body" data-trigger="hover" data-placement="top" data-content="Editar" data-original-title="">  </a> 
								<?php } else { ?>
									<a href="cd_pedido.php?cod=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_md5'];?>
&cli=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cliente'];?>
&e=s" class="fa fa-eye fa-lg font-green popovers" style="text-decoration:none" data-container="body" data-trigger="hover" data-placement="top" data-content="Visualizar" data-original-title="">  </a>
								<?php }?> 
							</td>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><td align="center"> <a href="javascript:void(null);" class="fa fa-trash fa-lg font-red popovers" style="text-decoration:none" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_pedido'];?>
','<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cliente'];?>
','<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['data2'];?>
');" data-container="body" data-trigger="hover" data-placement="top" data-content="Excluir" data-original-title="">  </a> </td><?php }?>
						</tr>
						<?php endfor; endif; ?>
						
						
					</tbody>
				</table>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Pedido</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Pedido?</h4>
					<input type="hidden" name="id_pedido" id="del_id_pedido">
					<input type="hidden" name="id_cliente" id="del_id_cliente">
					<input type="hidden" name="data_pedido" id="del_data_pedido">

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<?php echo '<script'; ?>
>
		function ExibirItensPed(el) {
			var display = document.getElementById(el).style.display;
			if (display == "none")
				document.getElementById(el).style.display = 'block';
			else
				document.getElementById(el).style.display = 'none';
		}
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover(); 
			$('[data-toggle2="popover"]').popover(); 
		});	
	<?php echo '</script'; ?>
>

<!-- End Footer--><?php }} ?>
