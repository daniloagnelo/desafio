<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:08:04
         compiled from "../smarty/tpl/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11423506415da60788f3d888-94070877%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '224c69cba9a460d62d13b9019ab77c8c5e12e6bb' => 
    array (
      0 => '../smarty/tpl/home.tpl',
      1 => 1571162883,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11423506415da60788f3d888-94070877',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da60789020455_27606024',
  'variables' => 
  array (
    'roteiros' => 0,
    'visitados' => 0,
    'pedidos' => 0,
    'pedidos_rs' => 0,
    'pedidos_rs_week' => 0,
    'pedidos_rs_month' => 0,
    'AmChart' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da60789020455_27606024')) {function content_5da60789020455_27606024($_smarty_tpl) {?>
<div class="row">
	

	
	<!-- ##javascrip:void(null); -->
	<!-- ##roteiros do dia 1 -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 red-soft" href="list_roteiros.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo $_smarty_tpl->tpl_vars['roteiros']->value;?>
">0</span>
				</div>
				<div class="desc">Roteiro do Dia</div>
			</div>
		</a>
	</div>
	<!-- ##roteiros do dia 1 -->
	
	<!-- ##visitados do dia 2 -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 blue-steel" href="list_roteiros.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo $_smarty_tpl->tpl_vars['visitados']->value;?>
">0</span>
				</div>
				<div class="desc">Visitados no Dia</div>
			</div>
		</a>
	</div>
	<!-- ##visitados do dia 2 -->
	
	<!-- Pedidos do dia 3 -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 green-jungle" href="grid_pedidos.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo $_smarty_tpl->tpl_vars['pedidos']->value;?>
">0</span>
				</div>
				<div class="desc">Vendas do Dia</div>
			</div>
		</a>
	</div>
	<!-- Pedidos do dia 3 -->
	
	<!-- ##Pedidos do dia 4 R$ -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 green" href="grid_pedidos.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					R$ <span data-counter="counterup" data-value="<?php echo number_format($_smarty_tpl->tpl_vars['pedidos_rs']->value,2,",",".");?>
">0</span>
				</div>
				<div class="desc">Vendas do Dia</div>
			</div>
		</a>
	</div>
	<!-- ##Pedidos do dia 4 R$ -->
	
	<!-- ##Pedidos da semana 5 R$ -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 yellow-casablanca" href="grid_pedidos.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					R$ <span data-counter="counterup" data-value="<?php echo number_format($_smarty_tpl->tpl_vars['pedidos_rs_week']->value,2,",",".");?>
">0</span>
				</div>
				<div class="desc">Vendas da Semana</div>
			</div>
		</a>
	</div>
	<!-- ##Pedidos da semana 5 R$ -->
	
	<!-- ##Pedidos do mes 6 R$ -->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 purple" href="consulta_pedidos.php">
			<div class="visual">
				<i class="icon-basket"></i>
			</div>
			<div class="details">
				<div class="number">
					R$ <span data-counter="counterup" data-value="<?php echo number_format($_smarty_tpl->tpl_vars['pedidos_rs_month']->value,2,",",".");?>
">0</span>
				</div>
				<div class="desc">Vendas do Mês</div>
			</div>
		</a>
	</div>
	<!-- ##Pedidos do mes 6 R$ -->
	

	
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart font-dark hide"></i>
					<span class="caption-subject font-dark bold uppercase">Vendas por Produto Dia</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_7" class="chart" style="height: 500px;"> </div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart font-dark hide"></i>
					<span class="caption-subject font-dark bold uppercase">Vendas por Produto Mês</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_8" class="chart" style="height: 500px;"> </div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-12 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart font-dark hide"></i>
					<span class="caption-subject font-dark bold uppercase">Vendas Mês + Ultimos 3 Meses</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_1" class="chart" style="height: 500px;"> </div>
			</div>
		</div>
	</div>
	

</div>



<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<!--script src="../includes/assets/pages/scripts/charts-amcharts.js" type="text/javascript"><?php echo '</script'; ?>
-->


<!-- script --------------------------------->

<?php echo $_smarty_tpl->tpl_vars['AmChart']->value;?>



<?php }} ?>
