<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:28:44
         compiled from "../smarty/tpl/cd_usuarios.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19949879045da60fdcb8be81-52280936%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '07e85e61b083503498b780132c9c7fc73fffde36' => 
    array (
      0 => '../smarty/tpl/cd_usuarios.tpl',
      1 => 1570450799,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19949879045da60fdcb8be81-52280936',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active' => 0,
    'inc' => 0,
    'adm' => 0,
    'alt' => 0,
    'exc' => 0,
    'list' => 0,
    'grupos' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da60fdcca0919_99716863',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da60fdcca0919_99716863')) {function content_5da60fdcca0919_99716863($_smarty_tpl) {?><div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['active']->value;?>
</span>
				</div>
				<div class="actions">
					<!--div class="btn-group btn-group-devided" data-toggle="buttons">
						<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
							<input type="radio" name="options" class="toggle" id="option1">Actions</label>
						<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Settings</label>
					</div-->
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
								<button id="sample_editable_1_new" class="btn sbold green" data-target="#add" data-toggle="modal" onclick="document.getElementById('form-add').reset();"> 
									<i class="fa fa-plus"></i>
									Adicionar Usuário
								</button>
								<?php }?>
							</div>
						</div>
						<!--div class="col-md-6">
							<div class="btn-group pull-right">
								<button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;">
											<i class="fa fa-print"></i> Print </a>
									</li>
									<li>
										<a href="javascript:;">
											<i class="fa fa-file-pdf-o"></i> Save as PDF </a>
									</li>
									<li>
										<a href="javascript:;">
											<i class="fa fa-file-excel-o"></i> Export to Excel </a>
									</li>
								</ul>
							</div>
						</div-->
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
					<thead>
						<tr>
							<th style="display:none">
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
									<span></span>
								</label>
							</th>
							<th class="col-md-2"> Login </th>
							<th class="col-md-5"> Nome </th>
							<th class="col-md-5"> Email </th>
							<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><th class=""></th><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><th class=""></th><?php }?>

						</tr>
					</thead>
					<tbody>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<tr class="odd gradeX">
							<td style="display:none" id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
_0">
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="checkboxes" value="1" />
									<span></span>
								</label>
							</td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
_1"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
 </td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
_2"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['nome'];?>
 </td>
							<td id="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
_3"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['email'];?>
 </td>
							<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><td align="center"> <a href="#" class="fa fa-edit fa-lg font-blue" style="text-decoration:none" data-toggle="modal" data-target="#edit" onclick="xajax_popula_edit('<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
');">  </a> </td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?><td align="center"> <a href="#" class="fa fa-trash fa-lg font-red" style="text-decoration:none" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['usr'];?>
');">  </a> </td><?php }?>
						</tr>
						<?php endfor; endif; ?>
						
						
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Add-->

<div class="modal fade bs-modal-lg" id="add" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-add" id="form-add">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-add',['add-grupo','add-descricao'], 'add');deleteRow('add');"></button>
					<h4 class="modal-title">Adicionar Usuário</h4>
				</div>
				<div class="modal-body">
				
					<div class="alert alert-danger display-hide" id="div-msg-add">
						<button class="close" data-close="alert"></button>
						<span id="msg-add"></span>
					</div>
					
					<div class="form-group" id="add-usr">
						<label>Usuário</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="usr" id="add_usr" class="form-control uppercase" placeholder="Usuário para login" > </div>
					</div>
					
					<div class="form-group" id="add-nome">
						<label>Nome</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="nome" id="add_nome" class="form-control" placeholder="Nome do Usuário"> </div>
					</div>
					
					<div class="form-group" id="add-email">
						<label>Email</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="email" id="add_email" class="form-control" placeholder="Email de contato"> </div>
					</div>
					
					<div class="form-group" id="add-cod_kronos">
						<label>Cód. Kronos</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="cod_kronos" id="add_cod_kronos" class="form-control" placeholder="Código Kronos"> </div>
					</div>
					
					<div class="form-group">
						<label>Permissões</label>
						
						<div class="mt-checkbox-inline">
							
							<span class="col-sm-4">
								<span class="pull-left">Administrador</span>
								<span class="pull-right">
									<input value="1" name="adm" id="add_adm" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Inclusão </span>
								<span class="pull-right">
									<input value="1" name="inc" id="add_inc" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Alteração </span>
								<span class="pull-right">
									<input value="1" name="alt" id="add_alt" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Exclusão </span>
								<span class="pull-right">
									<input value="1" name="exc" id="add_exc" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
						
						</div>
					</div>
					
					<div class="form-group">
						<label for="single-prepend-text" class="control-label">Grupo</label>
						<div class="row">
							
							<div class="col-sm-12">
								<div class="form-group col-sm-10">
								
									<div class="input-group select2-bootstrap-prepend " id="teste">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
												<span class="glyphicon glyphicon-search"></span>
											</button>
										</span>
										<select id="single-prepend-text" class="form-control select2-allow-clear">
											<option></option>
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['grupos']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['grupo'];?>
-<?php echo mb_convert_encoding($_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'], 'ISO-8859-1', "UTF-8");?>
" id="<?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['grupo'];?>
"><?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'];?>
</option>
											<?php endfor; endif; ?>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-2">
									
									<button class="btn sbold green" onclick="add_inc_grupo(getElementById('single-prepend-text'));" > 
										<i class="fa fa-plus"></i>
										Grupo
									</button>
								</div>
								
							</div>
						</div>
					</div>
					
	
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="add-tbgrupo">
								<tr>
									<th class="col-md-4"> Grupo </th>
									<th class="col-md-8"> Descrição </th>									
									<th class=""></th>
								</tr>

						</table>
					</div>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-add',['add-usr','add-nome', 'add-email'], 'add');deleteRow('add');">Fechar</button>
					<button type="button" class="btn green" onclick="xajax_add(xajax.getFormValues('form-add'));">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Add-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Edit-->


<div class="modal fade bs-modal-lg" id="edit" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-edit" id="form-edit">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-edit',['edit-usr','edit-nome', 'edit-email'], 'edit');deleteRow('edit');"></button>
					<h4 class="modal-title">Editar Usuário</h4>
				</div>
				<div class="modal-body">
				
					<div class="alert alert-danger display-hide" id="div-msg-edit">
						<button class="close" data-close="alert"></button>
						<span id="msg-edit"></span>
					</div>
					
					<div class="form-group" id="edit-usr">
						<label>Usuário</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="usr2" id="edit_usr2" class="form-control uppercase" placeholder="Usuário para login" disabled> 
							<input type="hidden" name="usr" id="edit_usr"> </div>
					</div>
					
					<div class="form-group" id="edit-nome">
						<label>Nome</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="nome" id="edit_nome" class="form-control" placeholder="Nome do Usuário"> </div>
					</div>
					
					<div class="form-group" id="edit-email">
						<label>Email</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="email" id="edit_email" class="form-control" placeholder="Email de contato"> </div>
					</div>
					
					<div class="form-group" id="edit-cod_kronos">
						<label>Cód. Kronos</label>
						<div class="input-icon right">
							<i class="fa fa-exclamation"></i>
							<input type="text" name="cod_kronos" id="edit_cod_kronos" class="form-control" placeholder="Código Kronos"> </div>
					</div>
					
					<div class="form-group" id="edit-permissoes">
						<label>Permissões</label>
						
						<div class="mt-checkbox-inline">
							
							<span class="col-sm-4">
								<span class="pull-left">Administrador</span>
								<span class="pull-right"  id="edit-adm">
									<input value="1" name="adm" id="edit_adm" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Inclusão </span>
								<span class="pull-right">
									<input value="1" name="inc" id="edit_inc" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Alteração </span>
								<span class="pull-right">
									<input value="1" name="alt" id="edit_alt" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
							<span class="col-sm-4">
								<span class="pull-left">Exclusão </span>
								<span class="pull-right">
									<input value="1" name="exc" id="edit_exc" type="checkbox"> 
								</span>
							</span>
							<br>
							<br>
						
						</div>
					</div>
					
					<div class="form-group">
						<label for="single-prepend-text" class="control-label">Grupo</label>
						<div class="row">
							
							<div class="col-sm-12">
								<div class="form-group col-sm-10">
								
									<div class="input-group select2-bootstrap-prepend ">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" data-select2-open="single-prepend-text2">
												<span class="glyphicon glyphicon-search"></span>
											</button>
										</span>
										<select id="single-prepend-text2" class="form-control select2-allow-clear">
											<option id="nd"></option>
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['grupos']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['grupo'];?>
-<?php echo mb_convert_encoding($_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'], 'ISO-8859-1', "UTF-8");?>
" id="<?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['grupo'];?>
"><?php echo $_smarty_tpl->tpl_vars['grupos']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'];?>
</option>
											<?php endfor; endif; ?>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-2">
									
									<button class="btn sbold green" onclick="edit_inc_grupo(getElementById('single-prepend-text2'));"> 
										<i class="fa fa-plus"></i>
										Grupo
									</button>
								</div>
								
							</div>
						</div>
					</div>
					
	
					<div class="portlet-body" >
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="edit-tbgrupo">
								<tr>
									<th class="col-md-4"> Grupo </th>
									<th class="col-md-8"> Descrição </th>									
									<th class=""></th>
								</tr>

						</table>
					</div>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-edit',['edit-usr','edit-nome', 'edit-email'], 'edit');deleteRow('edit');">Fechar</button>
					
					<button type="button" class="btn green" onclick="xajax_edit(xajax.getFormValues('form-edit'));">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Edit-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Grupo</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Grupo?</h4>
					<input type="hidden" name="usr" id="del_usr">

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<?php echo '<script'; ?>
>
		function add_inc_grupo(x){
			// Captura a referéncia da tabela com id minhaTabela
            var table = document.getElementById("add-tbgrupo");
            // Captura a quantidade de linhas já existentes na tabela
            var numOfRows = table.rows.length;
            // Insere uma linha no fim da tabela.
            var newRow = table.insertRow(numOfRows);
			
			var aux = x.value.split("-");
			
			var grupo = aux[0];
			var descricao = aux[1];
			var i;
			var err = "N";
			
			
			for (i = 1; i <= numOfRows; i++) {
				
				if($('#grupo'+i).attr('value') == grupo){
					alert("Grupo adicionado Anteriormente.");
					err = "S";
				}
			}
			
			if(x.value != ""){
				if(err == "N"){
					//newRow.id = "grupo"+numOfRows;
					newRow.innerHTML = "<td>"+grupo+"</td><td>"+descricao+"<input type='hidden' name='grupo[]' id='grupo"+numOfRows+"' value='"+grupo+"'/></td><td><a class='fa fa-trash fa-lg font-red' style='text-decoration:none' onclick='remove(this)'>  </a></td>";
				}
			}
			
		}
		
		function edit_inc_grupo(x){
			// Captura a referéncia da tabela com id minhaTabela
            var table = document.getElementById("edit-tbgrupo");
            // Captura a quantidade de linhas já existentes na tabela
            var numOfRows = table.rows.length;
            // Insere uma linha no fim da tabela.
            var newRow = table.insertRow(numOfRows);
			
			var aux = x.value.split("-");
			
			var grupo = aux[0];
			var descricao = aux[1];
			var i;
			var err = "N";
			
			
			for (i = 1; i <= numOfRows; i++) {
				
				if($('#grupo'+i).attr('value') == grupo){
					alert("Grupo adicionado Anteriormente.");
					err = "S";
				}
			}
			
			if(x.value != ""){
			
				if(err == "N"){
					//newRow.id = "grupo"+numOfRows;
					newRow.innerHTML = "<td>"+grupo+"</td><td>"+descricao+"<input type='hidden' name='grupo[]' id='grupo"+numOfRows+"' value='"+grupo+"'/></td><td><a class='fa fa-trash fa-lg font-red' style='text-decoration:none' onclick='remove(this)'>  </a></td>";
				}
				
			}
			
			
		}
		
		function deleteRow(tipo){
			var table = document.getElementById(tipo+"-tbgrupo");
			table.innerHTML = "<tr><th class='col-md-4'> Grupo </th><th class='col-md-8'> Descrição </th><th class=''></th></tr>";
		}
				

		(function($) {
		  remove = function(item) {
			var tr = $(item).closest('tr');

			tr.fadeOut(400, function() {
			  tr.remove();  
			});

			return false;
		  }
		})(jQuery);
		
	<?php echo '</script'; ?>
>

<!-- End Footer--><?php }} ?>
