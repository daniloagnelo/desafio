<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:37:53
         compiled from "../smarty/tpl/perfil.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18530175115da61201281d78-31865569%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec0754454e019e6d7edf122f2b088138a6cf2f70' => 
    array (
      0 => '../smarty/tpl/perfil.tpl',
      1 => 1570563825,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18530175115da61201281d78-31865569',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'img_perfil' => 0,
    'foto' => 0,
    'usr_nome' => 0,
    'cargo' => 0,
    'active' => 0,
    'usr' => 0,
    'usr_email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da61201309956_26152945',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da61201309956_26152945')) {function content_5da61201309956_26152945($_smarty_tpl) {?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet ">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<!--img src="../includes/assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""--> 
					<img src="<?php echo $_smarty_tpl->tpl_vars['img_perfil']->value;
echo $_smarty_tpl->tpl_vars['foto']->value;?>
" class="img-responsive" alt="" id="foto-perfil"> 
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"> <?php echo $_smarty_tpl->tpl_vars['usr_nome']->value;?>
 </div>
					<div class="profile-usertitle-job"> <?php echo $_smarty_tpl->tpl_vars['cargo']->value;?>
 </div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="perfil.php">
								<i class="icon-settings"></i> 
								Configurações da conta
							</a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
			<!-- END PORTLET MAIN -->
			
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light ">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i>
								<span class="caption-subject font-blue-madison bold uppercase"><?php echo $_smarty_tpl->tpl_vars['active']->value;?>
</span>
							</div>
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1_1" data-toggle="tab">Informações Pessoais</a>
								</li>
								<li>
									<a href="#tab_1_2" data-toggle="tab">Alterar Foto</a>
								</li>
								<li>
									<a href="#tab_1_3" data-toggle="tab">Alterar Senha</a>
								</li>
							</ul>
						</div>
						<div class="portlet-body">
							<div class="tab-content">
							
								<!-- PERSONAL INFO TAB -->
								<div class="tab-pane active" id="tab_1_1">
									<div class="alert alert-danger display-hide" id="div-msg-perfil">
										<button class="close" data-close="alert"></button>
										<span id="msg-perfil"></span>
									</div>
									<form role="form" action="javascript:void(null);" name="form-perfil" id="form-perfil">
										<div class="form-group">
											<label class="control-label">Usuário</label>
											<input type="text" placeholder="Ex.: DANILO" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['usr']->value;?>
" disabled="" readonly="" disabled/> 
										</div>
										<div class="form-group" id="nome">
											<label class="control-label">Nome</label>
											<input type="text" placeholder="Ex: Danilo dos Santos Agnelo" value="<?php echo $_smarty_tpl->tpl_vars['usr_nome']->value;?>
" class="form-control" name="nome" disabled/> 
										</div>
										<div class="form-group" id="email">
											<label class="control-label">Email</label>
											<input type="text" placeholder="Ex.: usuario@dominio.com.br" value="<?php echo $_smarty_tpl->tpl_vars['usr_email']->value;?>
" class="form-control" name="email" disabled/> 
										</div>
										
										<div class="margiv-top-10">
											<!--a href="javascript:;" class="btn green" onclick="xajax_alterar_perfil(xajax.getFormValues('form-perfil'));"> Salvar Alteração </a-->
											<!--a href="javascript:;" class="btn default"> Cancelar </a-->
										</div>
									</form>
								</div>
								<!-- END PERSONAL INFO TAB -->
								
								<!-- CHANGE AVATAR TAB -->
								<div class="tab-pane" id="tab_1_2">
									<div class="alert alert-danger display-hide" id="div-msg-foto">
										<button class="close" data-close="alert"></button>
										<span id="msg-foto"></span>
									</div>
									<form action="javascript:void(null);" name="form-file" id="form-file">
										<div class="form-group">
											<div class="fileinput fileinput-new" data-provides="fileinput">
											
												<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
													<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
												<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
												<div>
													<span class="btn default btn-file">
														<span class="fileinput-new"> Selecione uma Imagem </span>
														<span class="fileinput-exists"> Alterar </span>
														<input type="file" name="foto" id="foto" onchange="up_foto(this.value);"> 
														<input type="hidden" name="name_foto" id="name_foto"> 
													</span>
													<!--a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a-->
													
												</div>
											</div>
											<div class="clearfix margin-top-10">
												<span class="label label-danger"><b>Obs.!</b> </span>&nbsp;&nbsp;&nbsp;
												<span> A miniatura da imagem anexada á suportada nos últimos Firefox, Chrome, Opera, Safari e apenas no Internet Explorer 10. </span>
											</div>
										</div>
										<div class="margin-top-10">
											<!--a href="javascript:;" class="btn green" id="salvar_img"> Salvar </a-->
											<input type="submit" name="salvar_img" id="salvar_img" class="btn green" value="Salvar" onclick="xajax_alterar_foto(xajax.getFormValues('form-file'));"/>
											<!--a href="javascript:;" class="btn default"> Cancelar </a-->
										</div>
									</form>
								</div>
								<!-- END CHANGE AVATAR TAB -->
								
								<!-- CHANGE PASSWORD TAB -->
								<div class="tab-pane" id="tab_1_3">
									<div class="alert alert-danger display-hide" id="div-msg-password">
										<button class="close" data-close="alert"></button>
										<span id="msg-password"></span>
									</div>
									<form action="javascript:void(null);" name="form-password" id="form-password" autocomplete="off">
										<div class="form-group" id="senha_old">
											<label class="control-label">Senha Atual</label>
											<input type="password" class="form-control" autocomplete="off" name="senha_old" id="senha_old" required/> 
										</div>
										<div class="form-group" id="senha_new">
											<label class="control-label">Nova Senha</label>
											<input type="password" class="form-control" autocomplete="off" name="senha_new" id="senha_new" required/> 
										</div>
										<div class="form-group" id="senha_renew">
											<label class="control-label">Repetir a Nova Senha</label>
											<input type="password" class="form-control" autocomplete="off" name="senha_renew" id="senha_renew" required/> 
										</div> 
										<div class="margin-top-10">
											<a href="javascript:;" class="btn green" onclick="xajax_alterar_senha(xajax.getFormValues('form-password'));"> Alterar Senha </a>
											<!--a href="javascript:;" class="btn default"> Cancelar </a-->
										</div>
									</form>
								</div>
								<!-- END CHANGE PASSWORD TAB -->
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>



	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
 src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"><?php echo '</script'; ?>
>
	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
>
		//$(document).ready(function()
		//{
		//	
		//
		//	$("#foto").change(function(){
		//
		//		$('#form-file').ajaxForm({ 
		//			//url: 'processa-formulario.php',
		//			url: '../includes/funcoes_uteis.inc.php',
		//
		//			type: 'post',	
		//
		//			success: function(data){
		//				
		//				$('#name_foto').attr('value',data);
		//				
		//			}
		//
		//		}).submit();
		//		
		//	});
		//	 
		//
		//})
		
		function up_foto(file){
			$('#form-file').ajaxForm({
				url: '../includes/funcoes_uteis.inc.php',
				type: 'post',
				success: function(data){
					$('#name_foto').attr('value',data);
				}
			}).submit();
		};
		
	<?php echo '</script'; ?>
>
<?php }} ?>
