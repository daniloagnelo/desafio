<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-05-16 22:35:48
         compiled from "../smarty/tpl/cd_contato.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1015497065ec03f29167175-74110422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c475186c8bcc16c5afa449f112f88a8759be897e' => 
    array (
      0 => '../smarty/tpl/cd_contato.tpl',
      1 => 1589679337,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1015497065ec03f29167175-74110422',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ec03f292c1ff7_36953570',
  'variables' => 
  array (
    'titulo' => 0,
    'id_md5' => 0,
    'id_contato' => 0,
    'tipo' => 0,
    'nome' => 0,
    'idade' => 0,
    'list_tel' => 0,
    'tipoPessoa' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec03f292c1ff7_36953570')) {function content_5ec03f292c1ff7_36953570($_smarty_tpl) {?>
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>


<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-user font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="<?php echo $_smarty_tpl->tpl_vars['id_md5']->value;?>
"> 
					<input type="hidden" name="id_contato" id="id_contato" value="<?php echo $_smarty_tpl->tpl_vars['id_contato']->value;?>
"> 
					<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
						<button class="close" data-close="alert"></button>
						<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
					</div>
					
					<div class="row">						
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome">
								<label>Nome<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="nome" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_nome" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
" > 
								</div>
							</div>								
						</div>							
						<div class="col-sm-2">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-idade">
								<label>Idade</label>
								<div class="input-icon right">
									<input type="text" name="idade" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_idade" class="form-control" maxlength="2" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['idade']->value;?>
" > 
								</div>
							</div>								
						</div>					
					</div>
					
					<?php if ($_smarty_tpl->tpl_vars['tipo']->value=='edit') {?>
					<hr>
					<div class="row">						
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-telefone">
								<label>Telefone<span class="required"> * </span></label>
								<div class="input-icon right">
									<input type="text" name="telefone" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_telefone" class="form-control" maxlength="15" onkeydown="javascript: fMasc( this, mTel );"  placeholder="Informe um telefone ex: (16) 99793-4566"> 
								</div>
							</div>								
						</div>			
						<div class="col-sm-2">
							<div class="form-group col-sm-12" >
								<label>&nbsp;</label>
								<div class="input-icon right">
									<button type="button" id="btn_salvar_tel" class="btn green" onclick="xajax_incTel(document.getElementById('id_contato').value,document.getElementById('<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_telefone').value,'<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');"><i class="fa fa-plus"></i> Adcionar</button>
								</div>
							</div>								
						</div>		
					</div>
					<hr>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								
								<!--th class="col-md-2 text-center"> # </th-->
								<th style="display:none"></th>
								<th class="col-md-10 text-center">Telefone</th>
								<th class="col-md-2 text-center">Excluir</th>
							</tr>  
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list_tel']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<tr>
									<td style="display:none">
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1">
											<span></span>
										</label>
									</td>
									<!--td><?php echo $_smarty_tpl->tpl_vars['list_tel']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id'];?>
</td-->
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['list_tel']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['numero'];?>
</td>
									<td class="text-center"><a href="javascript:void(null);" class="fa fa-trash fa-lg font-red" style="text-decoration:none" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list_tel']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['id_contato']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['list_tel']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['numero'];?>
');" data-toggle2="popover" data-placement="top" data-trigger="hover" title="" data-content="Excluir" data-original-title="">  </a></td>
								</tr>
							<?php endfor; endif; ?>
						</tbody>
					</table>
					<?php }?>
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-idade'], '<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');window.location.href='grid_contatos.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-envio'), 0);topo();">Salvar</button>
					</div>
				</form>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Telefone</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Telefone?</h4>
					<input type="hidden" name="id_telefone" id="del_id_telefone">
					<input type="hidden" name="numero_telefone" id="del_numero_telefone">
					<input type="hidden" name="id_contato" id="del_id_contato">

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->


<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	
	<?php echo '<script'; ?>
>
		
	<?php echo '</script'; ?>
>	

<?php echo $_smarty_tpl->tpl_vars['tipoPessoa']->value;?>

	
<!-- End Footer--><?php }} ?>
