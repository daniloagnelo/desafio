<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:44:36
         compiled from "../smarty/tpl/cd_pedido.tpl" */ ?>
<?php /*%%SmartyHeaderCode:616972445da6139490f307-48965601%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '72d9b92d1acee33a87f2dba7a7c4d775c62531d5' => 
    array (
      0 => '../smarty/tpl/cd_pedido.tpl',
      1 => 1571141300,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '616972445da6139490f307-48965601',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    'inc' => 0,
    'adm' => 0,
    'tipo' => 0,
    'id_pedido' => 0,
    'exibir' => 0,
    'list_clientes' => 0,
    'cli' => 0,
    'obs_ped' => 0,
    'total_ped' => 0,
    'FinalizarPedido' => 0,
    'freteRetornoTpl' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da613949e0a72_88467890',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da613949e0a72_88467890')) {function content_5da613949e0a72_88467890($_smarty_tpl) {?>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	
	<style>
		.corpoProduto{
			border: 1px solid #dfdfdf;
			p{$tipo}ing-top:5px; 
			p{$tipo}ing-bottom:5px;
		}
		.corProduto{
			font:15px Arial, Sans-serif;
			height:34px;
			line-height:34px;
			text-align: center;
		}
		.aVertical{
			font:15px Arial, Sans-serif;
			height:34px;
			line-height:34px;
			text-align: center;
		}
		.aVerticalFL{
			font:15px Arial, Sans-serif;
			height:34px;
			line-height:34px;
			text-align: center;
			float:left;
		}
		.aVerticalFR{
			font:15px Arial, Sans-serif;
			height:34px;
			line-height:34px;
			text-align: center;
			float:right;
		}
		.btnProduto{
			width:50px;
			margin-right:3px;
		}
		.mrgTop{
			margin-top: 30px;
		}
		.mrgTop span{
			margin-top: 15px;
		}
		.radioButton{
			width: 18px;
			height: 18px;
		}
	</style>






<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-note font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_pedido.php'"> 
							<i class="fa fa-plus"></i>
							Pedido 
						</button>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
" id="form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
					
						<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
							<button class="close" data-close="alert"></button>
							<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
						</div>
						
						<?php if ($_smarty_tpl->tpl_vars['id_pedido']->value!='') {?>
						<input type="hidden" name="id_pedido" value="<?php echo $_smarty_tpl->tpl_vars['id_pedido']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['exibir']->value=='S') {?>disabled<?php }?>>
						<?php }?>
						<!--input type="hidden" name="cliente" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_cliente"  value="<?php echo $_smarty_tpl->tpl_vars['id_pedido']->value;?>
"-->

						<div class="row">
							<div class="col-sm-12">
								
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-cliente">
									<label>Cliente</label>
									<div class="input-icon">
										
										<select class="bs-select form-control" name="cliente" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_cliente" data-show-subtext="true" data-live-search="true"  onchange="xajax_btnAddProduto(this.value,'<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');" <?php if ($_smarty_tpl->tpl_vars['exibir']->value=='S') {?>disabled<?php }?>>
											
											<option value="0">Selecione um Cliente</option>
											
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list_clientes']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['list_clientes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['id_cliente'];?>
" data-icon="icon-user" <?php if ($_smarty_tpl->tpl_vars['cli']->value==$_smarty_tpl->tpl_vars['list_clientes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['id_cliente']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['list_clientes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['nome'];?>
</option>
											<?php endfor; endif; ?>
											
										</select>
										
									</div>
								</div>
							
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-sm-12">
							
								<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-obs">
									<label>Observação</label>
									<div>
										<textarea name="obs" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_obs" class="form-control" rows="2" style="resize: none; " <?php if ($_smarty_tpl->tpl_vars['exibir']->value=='S') {?>disabled<?php }?>><?php if ($_smarty_tpl->tpl_vars['obs_ped']->value!='') {
echo $_smarty_tpl->tpl_vars['obs_ped']->value;
}?></textarea>
									</div>
								</div>
							
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group col-sm-6" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-modelo">
									<div class="input-icon right">
										<button type="button" class="btn green" id="btnAddProduto-S"  data-target="#add-prod" data-toggle="modal" onclick="xajax_buscaProdutos(xajax.getFormValues('form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
'));"  <?php if ($_smarty_tpl->tpl_vars['exibir']->value=='S') {?>disabled<?php }?>><i class="fa fa-plus"></i> &nbsp; Adicionar Produtos</button>
									</div>
								</div>
								<!--div class="form-group col-sm-6" >
										<a href="javascript:void(null);" onclick="xajax_exibeItensPedido('S');" id="btnExibeItensPedido-S"><u>Mostrar Itens</u></a>
										<a href="javascript:void(null);" onclick="xajax_exibeItensPedido('N');" id="btnExibeItensPedido-N" style="display:none;"><u>Ocultar Itens</u></a>
								</div-->
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-12">								
								<div class="form-group col-sm-12 " id="div-list-itens-ped" style="display:block;">
								<label style="background-color:#666; width:100%; height:25px;color:#ffffff;font-size:15px; text-align:center;">Itens do Pedido</label>
									<div id="list-itens-ped" style="overflow:auto;"><p align="center">Nenhum Item para o Pedido.</p></div>									
								</div>								
							</div>
						</div>
						

						<div class="row">
							<div class="col-sm-12">	

								<div class="form-group col-sm-12 " id="div-list-totais-ped" style="display:block;">
								<label style="background-color:#666666; width:100%; height:25px;color:#ffffff;font-size:15px; text-align:center;">Total do Pedido</label>
									<!--<div id="total-gpr-ped">
										
									</div>--> 
									<div id="total-ped" class="col-sm-12">
										<div class="col-sm-6 text-left"> Total </div>
										<div class="col-sm-6 text-right w3-hover-shadow"> R$ <?php echo $_smarty_tpl->tpl_vars['total_ped']->value;?>
 </div>
									</div>									
								</div>								
							</div>
						</div>


					<div class="modal-footer">
						<button type="button" id="btn_fechar" class="btn dark btn-outline" data-dismiss="modal" onclick="window.location.href='grid_pedidos.php'">Fechar</button>
						<?php if ($_smarty_tpl->tpl_vars['exibir']->value=='N') {?><button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
'));topo();">Salvar</button><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['tipo']->value=='edit') {
if ($_smarty_tpl->tpl_vars['exibir']->value=='N') {?><button type="button" id="btn_finalizar" class="btn green" onclick="xajax_FinalizarPedido(xajax.getFormValues('form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
'));">Finalizar</button><?php }
}?>
						
					</div>
				</form>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


<!-- --------------------------------------------------------------------------------------------------------------------------- -->
<!-- Inicio Modal Produtos -->
<div class="modal fade bs-modal-lg" id="add-prod" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpaProduto('S');"></button>
					<h4 class="modal-title">Produtos</h4>
				</div>
				<div class="modal-body">
				
					<div class="alert alert-danger display-hide" id="div-msg-add-prod">
						<button class="close" data-close="alert"></button>
						<span id="msg-add"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="portlet-body">
								<div class="panel-group accordion" id="accordion">
									
									<form role="form" action="javascript:void(null);" name="form-add-prod" id="form-add-itens-prod">
										<div class="form-group col-12 " id="div-list-itens">
											
											<div class="form-group col-12" id="list-itens">
												
												
												
											</div>
											
										</div>
									</form>	
								</div>
							</div>
						</div>
					</div>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpaProduto('S');">Fechar</button>
					<button type="button" class="btn green" data-dismiss="modal" onclick="xajax_addProduto(xajax.getFormValues('form-add-prod'), xajax.getFormValues('form-add-itens-prod'), xajax.getFormValues('form-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
'));">Salvar</button>
				</div>
			
		</div>
	</div>
</div>
<!-- Fim Modal Produtos -->


<!-- --------------------------------------------------------------------------------------------------------------------------- -->
<!-- Inicio Modal Produtos -->
<div class="modal fade bs-modal-sm" id="qtditem" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpaMQtdItem();"></button>
					<h4 class="modal-title">Alterar Quantidade</h4>
				</div>
				<div class="modal-body">
				
					<div class="alert alert-danger display-hide" id="div-msg-qtditem">
						<button class="close" data-close="alert"></button>
						<span id="msg-qtditem"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="portlet-body">
								<div class="panel-group accordion" id="accordion">
									
									
									<form role="form" action="javascript:void(null);" name="form-qtditem" id="form-qtditem">
										
										<input type="hidden" name="p_item" id="p_item" value="">
										<input type="hidden" name="p_produto" id="p_produto" value="">
										<div class="row">
											
											<div class="form-group col-sm-12" id="div_quantidade">
												<label>Quantidade<span class="required"> * </span></label>
												<div class="input-icon right">
													
													<input type="text" name="p_quantidade" id="p_quantidade" class="form-control" placeholder="" onchange="xajax_calculaTotal(this.value,document.getElementById('p_produto').value);"> 
												</div>
											</div>
											
										</div>
										
										
									</form>										
									
								</div>
							</div>
						</div>
					</div>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpaMQtdItem();">Fechar</button>
					<button type="button" class="btn green" data-dismiss="modal" onclick="xajax_AlteraQtdeItem(xajax.getFormValues('form-qtditem'));">Salvar</button>
				</div>
			
		</div>
	</div>
</div>
<!-- Fim Modal Produtos -->

<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<?php echo '<script'; ?>
>
		function getMP( t ){
			
			
			var preco_total = $(t).data('precototal');
			var nome_cliente = $(t).data('nomecliente');
			var cpf = $(t).data('cpf'); 
			var email = $(t).data('email');
			var nr_pedido_kronos = $(t).data('nrpedidokronos');
			var ddd_telefone = $(t).data('dddtelefone ');
			var telefone = $(t).data('telefone');
			var endereco = $(t).data('endereco');
			var numero = $(t).data('numero ');
			var complemento = $(t).data('complemento');
			var cep = $(t).data('cep');
			var cidade = $(t).data('cidade');
			var estado = $(t).data('estado');
			var c = $(t).data('c');
			
			
			
			$(t).button('loading');
			$.get('mp/get.link.pagamento.php' , {
				
				v_preco_total : preco_total
				, v_nome_cliente : nome_cliente
				, v_cpf : cpf
				, v_email : email
				, v_nr_pedido_kronos : nr_pedido_kronos
				, v_ddd_telefone : ddd_telefone
				, v_telefone : telefone
				, v_endereco : endereco
				, v_numero : numero
				, v_complemento : complemento
				, v_cep : cep
				, v_cidade : cidade
				, v_estado : estado
				, v_c : c
				
				} , function(data){
					var url = data;
				$.get('atu.pedido.pagamento.php' , { v_nr_pedido_kronos :nr_pedido_kronos , v_url : url  }, function(data){
					document.location = url ;
				});
			});
			
		}
		
		function float2moeda(num) {

		   num = num.replace(".","");  //remove o ponto
		   num = num.replace(",","."); //transforma virgula em ponto

		   x = 0;

		   if(num<0) {
			  num = Math.abs(num);
			  x = 1;
		   }
		 if(isNaN(num)) num = "0"; 
			  cents = Math.floor((num*100+0.5)%100);

		   num = Math.floor((num*100+0.5)/100).toString();

		   if(cents < 10) cents = "0" + cents;
			  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
				 num = num.substring(0,num.length-(4*i+3))+'.'
					   +num.substring(num.length-(4*i+3));
		  ret = num + ',' + cents;
		   if (x == 1) ret = ' - ' + ret;return ret;

		}

		xajax_gridItensPedido();
		
		
	<?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"><?php echo '</script'; ?>
>
	<!--script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"><?php echo '</script'; ?>
-->
	<?php echo '<script'; ?>
>
		function pagamento_cc(code,ped){
			//var code = '1F69A3CF7878ED9994B3DF9DDC706796';
			var callback = {
				success : function(transactionCode) {
					//Insira os comandos para quando o usuário finalizar o pagamento. 
					//O código da transação estará na variável "transactionCode"
					console.log("Compra feita com sucesso, código de transação: " + transactionCode);
					xajax_salva_ps(code,ped,transactionCode);
				},
				abort : function() {
					//Insira os comandos para quando o usuário abandonar a tela de pagamento.
					console.log("abortado");
				}
			};
			//Chamada do lightbox passando o código de checkout e os comandos para o callback
			var isOpenLightbox = PagSeguroLightbox(code, callback);
			// Redireciona o comprador, caso o navegador não tenha suporte ao Lightbox
			if (!isOpenLightbox){
				location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code=" + code;
			}
		}
		
	<?php echo '</script'; ?>
>

<?php echo $_smarty_tpl->tpl_vars['FinalizarPedido']->value;?>


<?php if ($_smarty_tpl->tpl_vars['tipo']->value=="edit") {?>

	<?php echo $_smarty_tpl->tpl_vars['freteRetornoTpl']->value;?>


<?php }?>

<!-- End Footer--><?php }} ?>
