<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-05-16 16:12:39
         compiled from "../smarty/tpl/grid_clientes_card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11662553255ec03b279ccd63-81156837%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a07003c86d6d695466e545abcbefdb0de94e196' => 
    array (
      0 => '../smarty/tpl/grid_clientes_card.tpl',
      1 => 1589645279,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11662553255ec03b279ccd63-81156837',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active' => 0,
    'inc' => 0,
    'adm' => 0,
    'list' => 0,
    'alt' => 0,
    'exc' => 0,
    'count_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ec03b27b5a3a0_27244916',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec03b27b5a3a0_27244916')) {function content_5ec03b27b5a3a0_27244916($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/saaelpolitica/www/danilo/includes/libSmarty/plugins/modifier.truncate.php';
?><style>
	.thHand{
		cursor:pointer;	
	}
</style>
<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['active']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
						<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_cliente.php'"> 
							<i class="fa fa-plus"></i>
							Cliente 
						</button>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								
								
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
						<div class="form-group col-sm-12" id="add-pesquisar">
							<input type="text" name="pesquisar" id="pesquisar" class="form-control" maxlength="100" placeholder="Pesquisar" onkeyup="xajax_PesquisarCliente(this.value);"> 
						</div>
						<div class="form-group col-sm-12 text-center" id="add-pesquisar">
							OU
						</div>
						<div class="form-group col-sm-12" id="add-pesquisar_semana">
							<select class="bs-select form-control" data-show-subtext="true" data-style="btn green" name="dia_visita" onchange="xajax_PesquisarClienteSemana(this.value);">
								<option value="null" >Selecione um dia da Semana</option>
								<option value="SEG"  >Segunda</option>
								<option value="TER"  >Terça</option>
								<option value="QUA"  >Quarta</option>
								<option value="QUI"  >Quinta</option>
								<option value="SEX"  >Sexta</option>
								<option value="SAB"  >Sábado</option>
							</select> 
						</div>		
				</div>
				<HR>
                <div class="row" id="list_clientes">
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<div class="col-md-4" style="margin-bottom:40px;">
							<div class="dashboard-stat dashboard-stat-v2" style="margin:0;padding:0;border: 1px #E1E5EC solid;">
								<div class="visual">
									<!--i class="icon-globe font-default"></i-->
									<i class="font-default" style="font-size: 80px;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_cliente'];?>
</i> 
								</div>
								<div class="mt-body">
										<p class="text-right" style="color: #666;font-size: 15px;margin-top: 10px;padding: 0 10px;">
											<b><?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['nome'];?>
</b>
										</p>
										<p class="text-right" style="color: #666;font-size: 12px;margin-top: 10px;padding: 0 10px;">
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['contato']!='') {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['contato'];
}?>&nbsp;&nbsp;<i class="icon-user"></i><br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['telefone']!='') {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['telefone'];
}?>&nbsp;&nbsp;<i class="icon-call-end"></i><br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['celular']!='') {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['celular'];
}?>&nbsp;&nbsp;<i class="icon-screen-smartphone"></i><br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['endereco']!='') {
if (preg_match_all('/[^\s]/u',$_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['endereco'], $tmp)>26) {
echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['endereco'],26,"...",true);
} else {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['endereco'];
}?>, <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['numero'];
}?>&nbsp;&nbsp;<i class="icon-pointer"></i><br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cidade']!='') {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['cidade'];?>
/<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['uf'];
}?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['hora_abertura']!='') {?>Atendimento das <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['hora_abertura'];?>
 às <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['hora_fechamento'];
}?>&nbsp;&nbsp;<i class="icon-clock"></i><br>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='SEG') {?>Segunda<?php }?>
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='TER') {?>Terça<?php }?>												
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='QUA') {?>Quarta<?php }?>												
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='QUI') {?>Quinta<?php }?>												
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='SEX') {?>Sexta<?php }?>												
											<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dia_visita']=='SAB') {?>Sábado<?php }?>
											 - <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['periodo_visita'];?>
&nbsp;&nbsp;<i class="icon-calendar"></i><br>
											
										</p>
											
									
								</div>
							</div>
							<div class="mt-widget-2" style="margin:0;padding:0">
								<div class="mt-body" style="margin:0;padding:0">
									<div class="mt-body-actions">
										<div class="btn-group btn-group btn-group-justified">
											<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
											<a href="cd_cliente.php?C=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_md5'];?>
" class="btn font-blue">
												<i class="fa fa-edit fa-lg"></i> editar 
											</a>
											<?php }?>
											<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
											<a href="javascript:;" class="btn  font-red" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_cliente'];?>
');">
												<i class="fa fa-trash fa-lg"></i> excluir 
											</a>
											<?php }?>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						
						<?php endfor; endif; ?>
						<?php if ($_smarty_tpl->tpl_vars['count_list']->value==0) {?>
							<h4 align="center">Nenhum Cliente Cadastrado</h4>
						<?php }?>
					
				</div>
			   
			   
			</div>
		
		
		<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Cliente</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Cliente?</h4>
					<input type="hidden" name="id_cliente" id="del_id_cliente">

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<?php echo '<script'; ?>
>
	
	
	<?php echo '</script'; ?>
>

<!-- End Footer--><?php }} ?>
