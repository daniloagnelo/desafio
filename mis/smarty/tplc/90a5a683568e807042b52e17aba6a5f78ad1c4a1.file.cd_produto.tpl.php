<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-05-27 22:57:21
         compiled from "../smarty/tpl/cd_produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12141475565eceea10886024-02652896%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90a5a683568e807042b52e17aba6a5f78ad1c4a1' => 
    array (
      0 => '../smarty/tpl/cd_produto.tpl',
      1 => 1590630937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12141475565eceea10886024-02652896',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5eceea1090c8b0_62369247',
  'variables' => 
  array (
    'titulo' => 0,
    'id_md5' => 0,
    'tipo' => 0,
    'img' => 0,
    'PATH_IMG_PRODUTOS' => 0,
    'imagem_default' => 0,
    'nome' => 0,
    'sku' => 0,
    'quantidade' => 0,
    'preco' => 0,
    'descricao' => 0,
    'list_categoria' => 0,
    'list_produto_categoria' => 0,
    'list_tel' => 0,
    'tipoPessoa' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eceea1090c8b0_62369247')) {function content_5eceea1090c8b0_62369247($_smarty_tpl) {?>
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>


<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="<?php echo $_smarty_tpl->tpl_vars['id_md5']->value;?>
"> 
					<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
						<button class="close" data-close="alert"></button>
						<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group text-center" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-img">
								<div class="form-group">
									<div class="fileinput fileinput-new" data-provides="fileinput">
									
										<div class="fileinput-new thumbnail" style="width: 100%; height: 200px;">
											<!--img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" style="width: 200px; height: 200px;" /--> 
											<img src="<?php if ($_smarty_tpl->tpl_vars['img']->value!='') {
echo $_smarty_tpl->tpl_vars['PATH_IMG_PRODUTOS']->value;
echo $_smarty_tpl->tpl_vars['img']->value;
} else {
echo $_smarty_tpl->tpl_vars['imagem_default']->value;
}?>" alt="" style="width: 100%; height: 200px;" /> 
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 200px;"> </div>
										<div style="margin-top:10px">
											<span class="btn default btn-file">
												<span class="fileinput-new"> Selecione uma Imagem </span>
												<span class="fileinput-exists"> Alterar </span>
												<input type="file" name="file_img_prod" id="file_img_prod" onchange="up_foto(this.value);"> 
												<input type="hidden" name="name_img_prod" id="name_img_prod"> 
											</span>
											<!--a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a-->
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome">
								<label>Nome <span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="nome" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_nome" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
" > 
								</div>
							</div>								
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-sku">
								<label>SKU<span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="sku" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_sku" class="form-control text-upper" maxlength="11" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['sku']->value;?>
" > 
								</div>
							</div>
							<div class="form-group col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-quantidade">
								<label>Quantidade</label>
								<div class="input-icon right">
									
									<input type="text" name="quantidade" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_quantidade" class="form-control text-upper" maxlength="11" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['quantidade']->value;?>
" > 
								</div>
							</div>
							<div class="form-group col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-preco">
								<label>Preço</label>
								<div class="input-icon right">
									
									<input type="text" name="preco" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_preco" class="form-control text-upper" maxlength="17" placeholder="" value="<?php echo number_format($_smarty_tpl->tpl_vars['preco']->value,2,',','.');?>
" > 
								</div>
							</div>
							
						</div>
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-descricao">
								<label>Descrição</label>
								<div class="input-icon right">
									<textarea name="descricao" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_descricao" class="form-control" rows="5" maxlength="500" style="resize: none;"><?php echo $_smarty_tpl->tpl_vars['descricao']->value;?>
</textarea>
								</div>
							</div>
							
						</div>
					</div>
					
					<?php if ($_smarty_tpl->tpl_vars['tipo']->value=='edit') {?>
					<hr>
					<div class="row">						
						<div class="col-sm-10">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-categoria">
									<select class="bs-select form-control" name="categoria" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_categoria" data-show-subtext="true" data-live-search="true">
											
										<option value="0">Selecione uma Categoria</option>
										
										<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list_categoria']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['list_categoria']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['codigo'];?>
" data-icon="icon-flag"><?php echo $_smarty_tpl->tpl_vars['list_categoria']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['nome'];?>
</option>
										<?php endfor; endif; ?>
										
									</select>
							</div>								
						</div>			
						<div class="col-sm-2">
							<div class="form-group col-sm-12" >
								<div class="input-icon right">
									<button type="button" id="btn_salvar_tel" class="btn green" onclick="xajax_incCategoria(<?php echo $_smarty_tpl->tpl_vars['sku']->value;?>
, document.getElementById('<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_categoria').value);"><i class="fa fa-plus"></i> Adcionar</button>
								</div>
							</div>								
						</div>		
					</div>
					<hr>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								
								<!--th class="col-md-2 text-center"> # </th-->
								<th style="display:none"></th>
								<th class="col-md-10 text-center">Categoria</th>
								<th class="col-md-2 text-center">Excluir</th>
							</tr>  
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list_produto_categoria']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<tr>
									<td style="display:none">
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1">
											<span></span>
										</label>
									</td>
									<!--td><?php echo $_smarty_tpl->tpl_vars['list_tel']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id'];?>
</td-->
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['list_produto_categoria']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['nome_categoria'];?>
</td>
									<td class="text-center"><a href="javascript:void(null);" class="fa fa-trash fa-lg font-red" style="text-decoration:none" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list_produto_categoria']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['codigo'];?>
');" data-toggle2="popover" data-placement="top" data-trigger="hover" title="" data-content="Excluir" data-original-title="">  </a></td>
								</tr>
							<?php endfor; endif; ?>
						</tbody>
					</table>
					<?php }?>
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-sku','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-quantidade','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-preco','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-descricao'], '<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');window.location.href='grid_produtos.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-envio'), 0);topo();">Salvar</button>
					</div>
				</form>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Categoria</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão da Categoria?</h4>
					<input type="hidden" name="codigo_categoria" id="del_codigo_categoria">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->


<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
 src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"><?php echo '</script'; ?>
>
	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
>
		
		function up_foto(file){
			$('#form-envio').ajaxForm({
				//url: '../includes/funcoes_uteis.inc.php',
				url: 'upload_img_prod.php',
				type: 'post',
				success: function(data){
					$('#name_img_prod').attr('value',data);
				}
			}).submit();
		};
	<?php echo '</script'; ?>
>

<?php echo $_smarty_tpl->tpl_vars['tipoPessoa']->value;?>

	
<!-- End Footer--><?php }} ?>
