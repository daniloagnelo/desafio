<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-10-15 15:40:35
         compiled from "../smarty/tpl/grid_produtos_card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17875279085da612a3146b22-38696665%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16d1197ef522638ecaf42592666c540a658a1c80' => 
    array (
      0 => '../smarty/tpl/grid_produtos_card.tpl',
      1 => 1570644529,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17875279085da612a3146b22-38696665',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active' => 0,
    'inc' => 0,
    'adm' => 0,
    'list' => 0,
    'PATH_IMG_PRODUTOS' => 0,
    'imagem_default' => 0,
    'alt' => 0,
    'exc' => 0,
    'count_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5da612a3253467_59544752',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da612a3253467_59544752')) {function content_5da612a3253467_59544752($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/palheirospalhoca/www/intranet/includes/libSmarty/plugins/modifier.truncate.php';
?><style>
	.thHand{
		cursor:pointer;	
	}
</style>
<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['active']->value;?>
</span>
				</div>
				<div class="actions">
					<?php if ($_smarty_tpl->tpl_vars['inc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
					<button id="sample_editable_1_new" class="btn sbold green" onclick="window.location.href='cd_produto.php'"> 
						<i class="fa fa-plus"></i>
						Produto 
					</button>
					<?php }?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								
								
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<div class="col-md-3" style="margin-bottom:30px;">
						<div class="mt-widget-2"  style="text-align:center;">
							<div class="" style="background-image: url('<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img']!='') {
echo $_smarty_tpl->tpl_vars['PATH_IMG_PRODUTOS']->value;
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img'];
} else {
echo $_smarty_tpl->tpl_vars['imagem_default']->value;
}?>');background-size: 100% 100%; width:200px;height:200px;margin-left: auto;margin-right: auto; margin-top:10px;"></div>
							
							<div class="mt-body" style="padding:0px;margin-top:-100px;">
								<h3 class="mt-body-title"> <?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['nome'];?>
</h3>
								
								<!--p class="mt-body-description"> <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'],100,"...",true);?>
 </p-->
								<div style="min-height: 110px;vertical-align:middle;">
									<p style="color: #666;font-size: 13px;margin-top: 10px;padding: 0 10px;"> <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['descricao'],150,"...",true);?>
 </p>
								</div>
								
								<ul class="mt-body-stats">
									<li class="font-green">
										<i class="fa fa-money fa-lg"></i> <?php echo number_format($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['preco'],2,",",".");?>

									</li>									
								</ul>
								<div class="mt-body-actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php if ($_smarty_tpl->tpl_vars['alt']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
										<a href="cd_produto.php?C=<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_md5'];?>
" class="btn font-blue">
											<i class="fa fa-edit fa-lg"></i> editar 
										</a>
										<?php }?>
										<?php if ($_smarty_tpl->tpl_vars['exc']->value==1||$_smarty_tpl->tpl_vars['adm']->value==1) {?>
										<a href="javascript:;" class="btn  font-red" data-target="#del" data-toggle="modal" onclick="xajax_popula_del('<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id_produto'];?>
');">
											<i class="fa fa-trash fa-lg"></i> excluir 
										</a>
										<?php }?>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<?php endfor; endif; ?>
					<?php if ($_smarty_tpl->tpl_vars['count_list']->value==0) {?>
						<h4 align="center">Nenhum Produto Cadastrado3</h4>
					<?php }?>
				</div>
				
			</div>
		
		
		<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Produto</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão do Produto?</h4>
					<input type="hidden" name="id_produto" id="del_id_produto">

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Footer-->
<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<?php echo '<script'; ?>
>
	
	
	<?php echo '</script'; ?>
>

<!-- End Footer--><?php }} ?>
