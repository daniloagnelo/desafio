<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-05-27 23:04:29
         compiled from "../smarty/tpl/cd_categoria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19426603085ecf1c2d1749f9-29165185%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ae9983aa6c6a603dd0bc5396ffb1ee7a312b274' => 
    array (
      0 => '../smarty/tpl/cd_categoria.tpl',
      1 => 1590631466,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19426603085ecf1c2d1749f9-29165185',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    'id_md5' => 0,
    'codigo' => 0,
    'tipo' => 0,
    'nome' => 0,
    'tipoPessoa' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ecf1c2d1cd5e2_07964433',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ecf1c2d1cd5e2_07964433')) {function content_5ecf1c2d1cd5e2_07964433($_smarty_tpl) {?>
<style>
	.desabilitado .dropdown-toggle{
		cursor: not-allowed!important;
		background-color: #eef1f5!important;
		color: #333!important;
		border: solid 1px #c2cad8!important;
	}
	.text-upper{text-transform: uppercase;}
</style>


<div class="row"> 
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-puzzle font-dark"></i>
					<span class="caption-subject bold uppercase"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						
					</div>
				</div>
			</div>
			<div class="portlet-body" id="conteudo">
				<form role="form" action="javascript:void(null);" name="form-envio" id="form-envio">
					
					
					<input type="hidden" name="id_md5" id="id_md5" value="<?php echo $_smarty_tpl->tpl_vars['id_md5']->value;?>
"> 
					<input type="hidden" name="codigo" id="codigo" value="<?php echo $_smarty_tpl->tpl_vars['codigo']->value;?>
"> 
					<div class="alert alert-danger display-hide" id="div-msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
">
						<button class="close" data-close="alert"></button>
						<span id="msg-<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
"></span>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group col-sm-12" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome">
								<label>Nome <span class="required"> * </span></label>
								<div class="input-icon right">
									
									<input type="text" name="nome" id="<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
_nome" class="form-control" maxlength="100" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
" > 
								</div>
							</div>								
						</div>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-envio',['<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-codigo','<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
-nome'], '<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
');window.location.href='grid_categoria.php'">Fechar</button>
						<button type="button" id="btn_salvar" class="btn green" onclick="xajax_<?php echo $_smarty_tpl->tpl_vars['tipo']->value;?>
(xajax.getFormValues('form-envio'), 0);topo();">Salvar</button>
					</div>
				</form>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<!-- Begin Modal Del-->

<div class="modal fade bs-modal-lg" id="del" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" action="javascript:void(null);" name="form-del" id="form-del">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="xajax_limpa('form-del',[0],'');"></button>
					<h4 class="modal-title">Deletar Categoria</h4>
				</div>
				<div class="modal-body">
				
					<h4 class="modal-title">Confirma a Exclusão da Categoria?</h4>
					<input type="hidden" name="codigo_categoria" id="del_codigo_categoria">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" onclick="xajax_limpa('form-del',[0],'');">Não</button>
					<button type="button" class="btn green" onclick="xajax_del(xajax.getFormValues('form-del'));">Sim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- End Modal Del-->


<?php echo $_smarty_tpl->getSubTemplate ('script_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
 src="../includes/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../includes/assets/pages/scripts/profile.min.js" type="text/javascript"><?php echo '</script'; ?>
>
	<!--------------------------------------------------------------------------------------------->
	<?php echo '<script'; ?>
>
		
		function up_foto(file){
			$('#form-envio').ajaxForm({
				//url: '../includes/funcoes_uteis.inc.php',
				url: 'upload_img_prod.php',
				type: 'post',
				success: function(data){
					$('#name_img_prod').attr('value',data);
				}
			}).submit();
		};
	<?php echo '</script'; ?>
>

<?php echo $_smarty_tpl->tpl_vars['tipoPessoa']->value;?>

	
<!-- End Footer--><?php }} ?>
